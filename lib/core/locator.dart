import 'package:codebase/core/network/rest_api.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

void setupLocator(){
  locator.registerSingleton<GlobalKey<NavigatorState>>(GlobalKey(debugLabel: "Main Navigator"));
  locator.registerFactory(() => DioHttpProvider());

}