import 'dart:io';

import 'package:codebase/core/base/app_exception.dart';
import 'package:dio/dio.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:mime/mime.dart';
import 'package:http_parser/http_parser.dart';

import 'dio_http.dart';

class DioHttpProvider {
  Future<dynamic> dioGetRequest(String urlPath, {Map<String, dynamic> params, Function(dynamic) onError}) async {
    print("dioGetRequest $urlPath");
    var responseJson;
    try {
      var res = await dio.get(
        urlPath,
        queryParameters: params,
      );

      responseJson = _returnResponse(res);
    } on DioError catch (_) {
      // print("No internet ${e.toString()}//\n${e.type}//\n${e.error}");
      throw FetchDataException(message: 'No Internet connection');
    }
    return responseJson;
  }

  Future<dynamic> dioPutRequest(String urlPath, {body, Map<String, dynamic> params}) async {
    print("dioPutRequest $urlPath $body");
    var responseJson;
    try {
      Response res = await dio.put(
        urlPath,
        data: body,
        queryParameters: params,
      );
      responseJson = _returnResponse(res);
    } on DioError catch (_) {
      print("No internet");
      throw FetchDataException(message: 'No Internet connection');
    }

    return responseJson;
  }

  Future<dynamic> dioPostRequest(String urlPath, {body, Map<String, dynamic> params}) async {
    print("dioPostRequest $urlPath $body");
    var responseJson;
    try {
      Response res = await dio.post(urlPath,
          data: body, queryParameters: params, options: Options(contentType: Headers.jsonContentType));

      print("NEMO POST RESPONSE ${res.statusCode}");

//      print("dioPost response ${res.toString()}");
      responseJson = _returnResponse(res);
    } on DioError catch (e) {
//      print("No internet: ${e.response.statusCode} ${e.response.data}  ${e.message}");

      if (e.response.statusCode == 403) {
        responseJson = e.response.data;
      } else {
        throw FetchDataException(message: 'No Internet connection');
      }
    }

    return responseJson;
  }

  Future<dynamic> dioDeleteRequest(String urlPath, {body, Map<String, dynamic> params}) async {
    var responseJson;
    try {
      Response res = await dio.delete(
        urlPath,
        data: body,
        queryParameters: params,
      );
      responseJson = _returnResponse(res);
    } on DioError catch (_) {
      print("No internet");
      throw FetchDataException(message: 'No Internet connection');
    }
    return responseJson;
  }

  Future<dynamic> dioUploadSingleFile(String urlPath, List<File> files, VoidCallback onStop,
      {Map<String, dynamic> params}) async {
     print('dioUploadSingleFile: ${basename(files[0].path)}');
    var responseJson;
    try {

//      FormData formData = FormData.from({
//        "name": "wendux",
//        "age": 25,
//        "file": await MultipartFile.fromFile("./text.txt",filename: "upload.txt")
//      });

      Response res = await dio.post(urlPath,
          data: FormData.fromMap({
//            "filename": basename(files[0].path),
            "file": await MultipartFile.fromFile(
              files[0].path,
              filename: basename(files[0].path),
                contentType: MediaType.parse(lookupMimeType(basename(files[0].path)))
            ),
          }),
//          queryParameters: params,
//          options: Options(
//            contentType: "multipart/form-data",
//          ),
          onSendProgress: (received, total) {
//          print("PROGRESS $received $total");

//            https://chatserverdev.boodskapper.com/api/v1/users/bdo%40boodskapper.com/upload-document
          //https://chatserverdev.boodskapper.com/api/v1/users/strong%40boodskapper.com/upload-document

        if (received == total) {
          onStop();
        }
        if (total != -1) {
          // print('dioUploadSingleFile-/-onSendProgress: ' + (received / total * 100).toStringAsFixed(0) + "%");
        }
      });

      responseJson = _returnResponse(res);
    } on DioError catch (e) {
      print("dioUploadSingleFile ${e.message}");
      throw FetchDataException(message: 'No Internet connection');
    }

    return responseJson;
  }

  Future<dynamic> download(String urlPath, String fileName,
      {Map<String, dynamic> params, Function(int) progressCallback}) async {
    String dir = '';
    if (Platform.isIOS) {
      // dir = '/private' + (await getApplicationDocumentsDirectory()).parent.path + '/Downloads';
      // dir = '/private' + (await getTemporaryDirectory()).path;
      dir = (await getTemporaryDirectory()).path;
    } else {
      dir = (await getExternalStorageDirectory()).parent.parent.parent.parent.path + '/Download';
    }
    print('dioDownload: $dir/$fileName');
    // print('dioDownload1111111: ${(await DownloadsPathProvider.downloadsDirectory).path}');

    var responseJson;
    try {
      String oldPercent = '0';
      Response res =
          await dio.download(urlPath, '$dir/$fileName', queryParameters: params, onReceiveProgress: (received, total) {
        if (total != -1) {
          // Chỉ lấy % là: 10, 20, 30, 40, 50, 60, 70, 80, 90, 100
          if (oldPercent != (received / total * 100).toStringAsFixed(0) &&
              int.parse((received / total * 100).toStringAsFixed(0)) % 10 == 0) {
            oldPercent = (received / total * 100).toStringAsFixed(0);
            progressCallback(int.parse(oldPercent));
            // print('dioDownload-/-onReceiveProgress: ' +
            //   oldPercent +
            //   "%");
          }
        }
      });
      responseJson = _downloadResponse(res);
    } on DioError catch (_) {
      print("No internet");
      throw FetchDataException(message: 'No Internet connection');
    }

    return responseJson;
  }

  dynamic _downloadResponse(Response response) {
    if (response == null) {
      throw FetchDataException(message: "DOWNLOAD ITME OUT");
    } else {
      switch (response.statusCode) {
        case 200:
          return response.data;
        default:
          throw FetchDataException(message: 'Download error');
      }
    }
  }

  dynamic _returnResponse(Response response) {
    print("_returnResponse $response");
    if (response == null) {
      throw FetchDataException(message: "FETCH DATA TIME OUT");
    } else {
      switch (response.statusCode) {
        case 200:
          return response.data;
        case 400:
          throw BadRequestException(errorCode: 400, message: response.data.toString());
          break;
        case 401:
          throw BadRequestException(errorCode: 401, message: response.data.toString());
          break;
        case 403:
          throw UnauthorisedException(errorCode: 403, message: response.data.toString());
          break;
        case 404:
          throw BadRequestException(errorCode: 404, message: response.data.toString());
          break;
        case 406:
          throw BadRequestException(errorCode: 406, message: response.data.toString());
          break;
        case 500:
          break;
        default:
          print("status code null");
          throw FetchDataException(message: 'Eror occured while Communication with Server');
      }
    }
  }
}
