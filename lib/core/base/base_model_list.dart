class BaseModelList<T> {
  int totalRows;
  int currentPage;
  int pageSize;

  BaseModelList(this.totalRows, this.currentPage, this.pageSize);

  BaseModelList.map(dynamic json){
    totalRows = json["totalRows"] == null ?  0 : json["totalRows"];
    currentPage = json["currentPage"] == null ?  0 : json["currentPage"];
    pageSize = json["pageSize"] == null ?  0 : json["pageSize"];
  }
}