class BaseResponse{
	int errorCode;
	String message;
	String version;

	BaseResponse(this.errorCode, this.message, this.version);

	BaseResponse.map(dynamic obj){
		this.errorCode = obj["errorCode"];
		this.message = obj["message"];
		this.version = obj["version"] == null? "": obj["version"];
	}
}