import 'package:bot_toast/bot_toast.dart';
import 'package:codebase/widget/dialog_loading.dart';
import 'package:flutter/material.dart';

class BaseView {
  ValueNotifier<bool> enableLoading = ValueNotifier<bool>(false);

  String message = '';
  bool needBackground;

  void showLoading({String msg = '', bool needBackground = true}) {
    enableLoading.value = true;
    this.needBackground = needBackground;

    this.message =  msg.isNotEmpty? msg: '';
  }

  void hideLoading() {
    enableLoading.value = false;
  }

  Widget loadingWidget() => ValueListenableBuilder<bool>(
        valueListenable: enableLoading,
        builder: (_, value, child) {
          return AnimationDialogLoading(
            showLoading: value,
            message: message,
            needGrayBackground: needBackground,
          );
        },
      );

  void showToast(String message) {
    BotToast.showText(text: message);
  }
}
