class MenuDrawerKey {
  static const String KEY_DRAWER_PROFILE = 'profile';
  static const String KEY_DRAWER_ABOUT = 'about';
  static const String KEY_DRAWER_PRIVACY = 'privacy';
  static const String KEY_DRAWER_TERMS = 'terms';
  static const String KEY_DRAWER_SUPPORT = 'support';
  static const String KEY_DRAWER_UPDATE = 'update';
  static const String KEY_DRAWER_LOGOUT = 'logout';
  static const String KEY_DRAWER_SETTINGS = 'settings';
}
