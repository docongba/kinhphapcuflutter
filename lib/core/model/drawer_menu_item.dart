class DrawerMenuItem {
  String key;
  String title;
  String icon;

  DrawerMenuItem({
    this.key,
    this.title,
    this.icon,
  });
}
