
class SuccessResponse{
  int status;
  String message;

  SuccessResponse(this.status, this.message);

  SuccessResponse.map(dynamic obj){
    this.status = obj["status"];
    this.message = obj["message"];
  }
}