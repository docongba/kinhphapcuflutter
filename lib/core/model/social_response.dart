
class SocialResponse{
  int status;
  String message;
  String hash;
  SocialUserData user;

  SocialResponse(this.status, this.message, this.user, this.hash);

  SocialResponse.map(dynamic obj){
    this.status = obj["status"];
    this.message = obj["message"];
    this.hash = obj['hash'];
    this.user = SocialUserData.map(obj['user']);
  }
}

class SocialUserData{

  String userId;
  String email;
  String firstName;
  String lastName;
  String phoneNumber;
  String customer;
  String role;
  String userRole;
  SocialUserData({this.phoneNumber, this.email, this.lastName, this.firstName, this.role, this.userRole, this.userId, this.customer});

  SocialUserData.map(dynamic obj){
    this.userId = obj["UserID"];
    this.email = obj["Email"];
    this.firstName = obj["FirstName"];
    this.lastName = obj["LastName"];
    this.phoneNumber = obj["PhoneNumber"];
    this.customer = obj["Customer"];
    this.role = obj["Role"];
    this.userRole = obj["UserRole"];
  }
}