class FilterOptionType{
  static const String STATUS = 'Status';
  static const String TYPE = 'Type';
  static const String INSPECTOR = 'Inspector';
  static const String METHOD = 'Method';
}