import 'dart:convert';
import 'dart:io';

import 'package:codebase/constant/app_constant.dart';
import 'package:codebase/manager/event_bus.dart';
import 'package:device_info/device_info.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DataInstance {

	static DataInstance _instance = new DataInstance.internal();

	DataInstance.internal();

	factory DataInstance() => _instance;

	double screenW, screenH;
	String sessionId = '';
	String accessToken;
	String deviceToken;
	String idToken;
	bool isLogin;
	String userId;
	String fullName = '';

	String authUrl;
	String baseUrl;
	String baseTagUrl;
	String shortBobBaseUrl;
	String appKey;
	String chatSocket;
	String chatSocketApi;
	EventBus eventBus = EventBus();
	double statusBarHeight;
	String deviceId;
	bool flag;

	checkDeviceInfo() async{
		DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
		if(Platform.isIOS){
			IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
			deviceId = iosInfo.identifierForVendor;
		}else{
			AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
			deviceId = androidInfo.id;
		}

		print('checkDeviceInfo $deviceId');
	}
	
	initPreference() async{
		SharedPreferences prefs = await SharedPreferences.getInstance();
		DataInstance().isLogin = prefs.getBool(PreferenceConstant.PREF_KEY_LOGIN)?? false;
		DataInstance().accessToken = prefs.getString(PreferenceConstant.PREF_KEY_ACCESS_TOKEN);
		DataInstance().sessionId = prefs.getString(PreferenceConstant.PREF_KEY_SOCKET_SESSION_ID)?? '';
		DataInstance().idToken = prefs.getString(PreferenceConstant.PREF_KEY_ID_TOKEN);
	}

	initData({String appKey, String clientId, String clientSecret, String authUrl, String baseUrl, String chatSocket, String chatSocketApi,
	String baseTagUrl, String shortBobBaseUrl, bool flag, String fbKey}) async{

		this.flag = flag;

		this.appKey = appKey;

		this.authUrl = authUrl;
		this.baseUrl = baseUrl;

		this.chatSocket = chatSocket;
		this.chatSocketApi = chatSocketApi;

		this.baseTagUrl = baseTagUrl;

		this.shortBobBaseUrl = shortBobBaseUrl;
	}
}
