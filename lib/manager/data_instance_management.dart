
import 'dart:convert';

import 'package:codebase/constant/app_constant.dart';
import 'package:codebase/manager/data_instance.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DataInstanceManagement {
  void saveToken({String checksum, String loginType, String access, String idToken, String userId}) async{
    DataInstance().accessToken = access;
    DataInstance().idToken = idToken;
    DataInstance().isLogin = true;
    DataInstance().userId = userId;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(PreferenceConstant.PREF_KEY_ACCESS_TOKEN, access);
    await prefs.setString(PreferenceConstant.PREF_KEY_ID_TOKEN, idToken);
    await prefs.setBool(PreferenceConstant.PREF_KEY_LOGIN, true);
    await prefs.setString(PreferenceConstant.PREF_LOGIN_TYPE, loginType);
  }
}