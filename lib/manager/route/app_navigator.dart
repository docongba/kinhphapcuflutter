import 'package:codebase/core/locator.dart';
import 'package:codebase/manager/route/route.dart';
import 'package:flutter/material.dart';

class AppNavigator {
  GlobalKey<NavigatorState> _globalKey = locator<GlobalKey<NavigatorState>>();

  void pop() {
    _globalKey.currentState.pop();
  }

//  void moveToNotificationSettings() {
//    _globalKey.currentState.pushNamed(RoutePaths.notificationSettingsScreen);
//  }
//
//  void moveToLogin() {
//    _globalKey.currentState.pushNamedAndRemoveUntil(RoutePaths.login, (route) => false);
//  }


}

