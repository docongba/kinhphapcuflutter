export 'assets_constant.dart';
export 'enum_constant.dart';
export 'properties_constant.dart';
export 'ui_constant.dart';

import 'package:codebase/utils/action_delay.dart';
import 'package:shared_preferences/shared_preferences.dart';

// final FirebaseMessaging firebaseMessaging = FirebaseMessaging();
SharedPreferences prefs = SharedPreferences.getInstance() as SharedPreferences;
Debouncer delayAction = Debouncer(milliseconds: 300);
final DateTime currentDate = DateTime.now();
const Duration loadingAnimatedDuration = Duration(milliseconds: 1000);

class PreferenceConstant{
  static const String PREF_KEY_ACCESS_TOKEN = "access_token";
  static const String PREF_KEY_SOCKET_SESSION_ID = "socket_session_id";
  static const String PREF_KEY_ID_TOKEN = "id_token";
  static const String PREF_KEY_LOGIN = "logged_in";
  static const String PREF_KEY_FIRST_LOGIN = 'first_login';
  static const String PREF_KEY_UNREAD_CONVERSATION = 'unread_conversation';
  static const String PREF_KEY_PROFILE = 'profile_data';

  static const String PREF_LOGIN_TYPE = 'login_type';
  static const String PREF_CHECKSUM = 'check_sum';
  static const String PREF_DEVICE = 'get_device';

  static const String PREF_KEY_NOTI_LOCATION_TRACKING = 'TrackingLocation';
  static const String PREF_KEY_NOTI_GROUP_CHANNEL = 'GroupChatRoom';
  static const String PREF_KEY_NOTI_FROM_BOB = 'BobNotification';
}




