
import 'package:codebase/utils/strings.dart';



class DateTimeFormat {
  static const String MMDDYYYY = "MM/dd/yyyy";
  static const String YYYYMMDD = "yyyy-MM-dd";
  static const String YYYYMM_DATE_FORMAT = "yyyy-MM";
  static const String YYYY_DATE_FORMAT = "yyyy";
  static const String DD_MM_YYYY = "dd-MM-yyyy";
  static const String MM_YYYY = "MM-yyyy";
  static const String YYYYMMDDHHMM_DATE_FORMAT = "yyyy-MM-dd HH:mm";
  static const String YYYYMMDDHHMM_DATE_FORMAT_UTC = "yyyy-MM-ddTHH:mm";
}

class FileTypeKey {
  static const String pdf = 'pdf';
  static const String doc = 'doc';
  static const String docx = 'docx';
  static const String xlsx = 'xlsx';
  static const String csv = 'csv';
}

class GenderKey {
  static const String male = 'M';
  static const String female = 'F';
  static const String other = 'O';
}

Map<String, String> genderData = {
    GenderKey.male : AppString.labelMale,
    GenderKey.female : AppString.labelFemale,
    GenderKey.other : AppString.labelOther,
};

