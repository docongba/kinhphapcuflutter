import 'package:codebase/constant/ui_constant.dart';
import 'package:codebase/theme/text_style.dart';
import 'package:flutter/material.dart';

import 'colors.dart';

class TextFieldUtils {
  static TextFieldUtils _instance = new TextFieldUtils.internal();

  TextFieldUtils.internal();

  factory TextFieldUtils() => _instance;

  double _radius = kDouble_8;

  OutlineInputBorder unBorder() {
    return OutlineInputBorder(
        borderSide: BorderSide(
          color: Colors.transparent,
        ),
      );
  }

  OutlineInputBorder borderOutlineError({Color color = kOrangeColor}) {
    return OutlineInputBorder(borderRadius: BorderRadius.circular(_radius), borderSide: BorderSide(color: color, width: 1.0));
  }

  OutlineInputBorder borderOutlineUnFocus({Color color = kGreyLineColor}) {
    return OutlineInputBorder(borderRadius: BorderRadius.circular(_radius), borderSide: BorderSide(color: color, width: 1.0));
  }

  OutlineInputBorder borderOutlineFocus({Color color = kGreyLineColor}) {
    return OutlineInputBorder(borderRadius: BorderRadius.circular(_radius), borderSide: BorderSide(color: color, width: 1.0));
  }

   unFocusTextField(BuildContext context) {
    return FocusScope.of(context).unfocus();
  }

  underlineInputBorderFocus() {
    return UnderlineInputBorder(borderSide: BorderSide(color: kGreenColor, width: 1.0));
  }

  underlineInputBorderUnFocus() {
    return UnderlineInputBorder(borderSide: BorderSide(color: kGreyLineColor, width: 1.0));
  }

  InputDecoration customInputDecoration({String error, String hint, Widget suffixIcon}) => InputDecoration(
        labelStyle: TextStyleCustom.regular14(),
        border: borderOutlineUnFocus(),
        enabledBorder: borderOutlineUnFocus(),
        focusedBorder: borderOutlineFocus(),
        filled: true,
        fillColor: kWhiteColor,
        hintStyle: TextStyle(
          color: kTextDisableColor,
        ),
        errorText: error,
        hintText: hint,
        suffixIcon: suffixIcon,
        isDense: true,
      );

  InputDecoration customInputDecorationWithPrefix({String error, String hint, Widget suffixIcon, Widget prefixIcon}) => InputDecoration(
    labelStyle: TextStyleCustom.regular14(),
    border: borderOutlineUnFocus(),
    enabledBorder: borderOutlineUnFocus(),
    focusedBorder: borderOutlineFocus(),
    filled: true,
    fillColor: kWhiteColor,
    hintStyle: TextStyle(
      color: kTextDisableColor,
    ),
    errorText: error,
    hintText: hint,
    suffixIcon: suffixIcon,
    prefixIcon: prefixIcon,
    isDense: true,
  );

  InputDecoration chatBox(String hint, {Widget suffixIcon}) => InputDecoration(
    labelStyle: TextStyleCustom.regular14(),
    border: borderOutlineUnFocus(),
    enabledBorder: borderOutlineUnFocus(),
    focusedBorder: borderOutlineFocus(),
    filled: true,
    fillColor: kWhiteColor,
    hintStyle: TextStyle(
      color: kTextDisableColor,
    ),
    hintText: hint,
    suffixIcon: suffixIcon,
    isDense: true,
  );

  InputDecoration customNormalInputDecoration(String hint, {Widget suffixIcon}) => InputDecoration(
    labelStyle: TextStyleCustom.regular14(),
    border: borderOutlineUnFocus(),
    enabledBorder: borderOutlineUnFocus(),
    focusedBorder: borderOutlineFocus(),
    filled: true,
    fillColor: kWhiteColor,
    hintStyle: TextStyle(
      color: kTextDisableColor,
    ),
    hintText: hint,
    suffixIcon: suffixIcon,
    isDense: true,
  );

  InputDecoration unBorderTextFormFieldDecoration({
  String hint = "",
  TextStyle hintStyle,
  Widget prefix,
  Widget suffix,
  Widget icon,
  Color color,
  Widget suffixIcon,
  EdgeInsets contentPadding
}) =>
    InputDecoration(
        hintText: hint,
      hintStyle: hintStyle == null
          ? TextStyle(
//              fontFamily: FontFamilyConstant.regular,
              fontSize: kDouble_16,
              height: 18 / 16,
              color: Color(0xFFB8B8B8),
            )
          : hintStyle,
      fillColor: color ?? kWhiteColor,
      contentPadding: contentPadding ?? const EdgeInsets.only(
          left: kDouble_16, top: kDouble_12, bottom: kDouble_12),
      filled: true,
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: Colors.transparent,
        ),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(
          color: Colors.transparent,
        ),
      ),
      suffix: suffix,
      suffixIcon: suffixIcon,
      icon:  icon,
      prefix: prefix
    );
}
