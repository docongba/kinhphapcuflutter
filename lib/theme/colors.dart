import 'package:flutter/material.dart';


/// - Color(`0xFF1768B2`)
const kPrimaryColor = const Color(0xFF00B0F0);

/// - Color(`0xFF034EA2`)
const kDarkBlueColor = const Color(0xFF034EA2);

/// - Color(`0xFFFF863d`)
const kOrangeColor = const Color(0xFFFF863D);

/// - Color(`0xFF6FC859`)
const kGreenColor = const Color(0xFF6FC859);

/// - Color(`0xFF222222`)
const kBlackColor = const Color(0xFF222222);

/// - Color(`0xFF797979`)
const kTextColor = const Color(0xFF3E3E3E);

const kGreyTextColor = const Color(0xFF747474);

/// - Color(`0xFFACACAC`)
const kTextDisableColor = const Color(0xFFACACAC);

/// - Color(`0xFFD0D0D0`)
const kGreyLineColor = const Color(0xFFD0D0D0);

/// - Color(`0xFFF1F1F1`)
const kBackgroundColor = const Color(0xFFF1F1F1);

/// - Color(`0xFFFFFFFF`)
const kWhiteColor = const Color(0xFFFFFFFF);

const kMenuItemColor = const Color(0xFF737373);

const kHeavyGreenColor = const Color(0xFF2F8980);

const kBlueColor = const Color(0xFF0084C6);

const kQuickReplyColor = const Color(0xFFa6e7ff);

const kGreenActiveColor = const  Color(0xFF0DDD3B);

const kRedColor = const Color(0xFFEE2A2A);

const kCaseManagementColor = const Color(0xFFC7DEEC);

const kAppleBlackColor = const Color(0xFF1C1C1C);

const kGoogleRedColor = const Color(0xFFDA5F4F);

const kIndicatorColor = const Color(0xFF963FDA);

const kFacebookRedColor = const Color(0xFF37528C);

const kLinkedInColor = const Color(0xFF0577B5);

const kEndGradient = const Color(0xFF4DF9F3);

const kStartGradient = const Color(0xFF00B0F0);

const kCaseQueueStatus = const Color(0xFFDA5F4F);

const kCaseUnreadStatus = const Color(0xFFDA5F4F);

const kCaseCompleteStatus = const Color(0xFF747474);

const kCaseActiveStatus = const Color(0xFFF4DD65);




