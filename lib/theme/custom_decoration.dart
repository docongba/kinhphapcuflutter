import 'package:codebase/constant/ui_constant.dart';
import 'package:codebase/theme/colors.dart';
import 'package:flutter/material.dart';

class CustomDecoration {

  static Decoration selectedAdditionalRoomDecoration({Color color = kWhiteColor, double radius = 6.0, Color borderColor = kPrimaryColor}) =>
      BoxDecoration(
        border: Border.all(color: borderColor, width: 2.0),
        color: color,
        borderRadius: BorderRadius.all(Radius.circular(radius)),
      );

  static Decoration tagDecoration({Color color = kWhiteColor, double radius = 6.0, Color borderColor = kPrimaryColor}) =>
      BoxDecoration(
        border: Border.all(color: borderColor, width: 2.0),
        color: color,
        borderRadius: BorderRadius.all(Radius.circular(radius)),
        boxShadow: [
          new BoxShadow(
              spreadRadius: 2.1, offset: Offset(0.1, 0.1), color: Colors.black.withOpacity(0.08), blurRadius: 8)
        ],
      );

  static Decoration cardDecoration({bool isBorder = false, Color color = kWhiteColor, double radius = 6.0}) =>
      BoxDecoration(
        border: isBorder ? Border.all(color: kGreyLineColor) : null,
        color: color,
        borderRadius: BorderRadius.all(Radius.circular(radius)),
        boxShadow: [
          new BoxShadow(
              spreadRadius: 0.08, offset: Offset(0.0, 0.0), color: Colors.black.withOpacity(0.08), blurRadius: 4)
        ],
      );

  static Decoration inspectionCardMap({bool isBorder = false, Color color = kWhiteColor, double radius = 8.0}) =>
      BoxDecoration(
        border: isBorder ? Border.all(color: kGreyLineColor) : null,
        color: color,
        borderRadius: BorderRadius.all(Radius.circular(radius)),
//        boxShadow: [
//          new BoxShadow(
//              spreadRadius: 0.5, offset: Offset(0.2, 0.2), color: Colors.black.withOpacity(0.1), blurRadius: 2)
//        ],
      boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.3),
          spreadRadius: 2,
          blurRadius: 8,
          offset: Offset(0, 2), // changes position of shadow
        ),
      ]
      );

  static shapeRoundedTab({double radius = 20.0, bool isLeft = true, Color color = kPrimaryColor, double borderSize = kDouble_1}) {
    return RoundedRectangleBorder(
        borderRadius: isLeft? BorderRadius.only(
          bottomLeft: const Radius.circular(40.0),
          topLeft: const Radius.circular(40.0),
        ): BorderRadius.only(
          bottomRight: const Radius.circular(40.0),
          topRight: const Radius.circular(40.0),
        ),
        side: BorderSide(width: borderSize, color: color));
  }

  static Decoration movingBottomLayout({bool isBorder = false, Color color = kWhiteColor, double radius = 6.0}) =>
      BoxDecoration(
        border: Border(),
        color: color,
        borderRadius: new BorderRadius.only(
          bottomLeft: Radius.circular(radius),
          bottomRight: Radius.circular(radius),
        ),
//        boxShadow: [
//          new BoxShadow(color: Colors.red, offset: new Offset(20.0, 10.0), blurRadius: 20.0, spreadRadius: 40.0)
//        ],
      );

  static Decoration explainMovingTopLayout({bool isBorder = false, Color color = kWhiteColor, double radius = 6.0}) =>
      BoxDecoration(
        border: Border(),
        color: color,
        borderRadius: new BorderRadius.only(
          topLeft: Radius.circular(radius),
          topRight: Radius.circular(radius),
        ),
//        boxShadow: [
//          new BoxShadow(color: Colors.red, offset: new Offset(20.0, 10.0), blurRadius: 20.0, spreadRadius: 40.0)
//        ],
      );

  static BoxDecoration avatarDecor({bool isBorder = false, Color color = kWhiteColor, double radius = 6.0}) =>
      BoxDecoration(
          border: isBorder ? Border.all(color: kGreyLineColor) : null,
          color: color,
//    borderRadius: BorderRadius.all(Radius.circular(radius)),
          shape: BoxShape.circle);

  static Decoration buttonBorderOnly(
          {Color borderColor = kPrimaryColor,
          Color color = kWhiteColor,
          double radius = 20.0,
          double borderWidth = 2.0}) =>
      BoxDecoration(
        border: Border.all(color: borderColor, width: borderWidth),
        color: color,
        borderRadius: BorderRadius.all(Radius.circular(radius)),
//    boxShadow: [
//      new BoxShadow(
//          spreadRadius: 0.08,
//          offset: Offset(0.0, 0.0), color: Colors.black.withOpacity(0.08), blurRadius: 4)
//    ],
      );

  static Decoration buttonBorder(
          {bool isBorder = false,
          Color color = kWhiteColor,
          double radius = 20.0,
          double borderWidth = 2.0,
          Color borderColor = kPrimaryColor}) =>
      BoxDecoration(
        border: isBorder ? Border.all(color: borderColor, width: borderWidth) : null,
        color: color,
        borderRadius: BorderRadius.all(Radius.circular(radius)),
//    boxShadow: [
//      new BoxShadow(
//          spreadRadius: 0.08,
//          offset: Offset(0.0, 0.0), color: Colors.black.withOpacity(0.08), blurRadius: 4)
//    ],
      );

  static Decoration chatBoxDecoration(
          {bool isBorder = false, Color color = kPrimaryColor, double radius = 20.0, double borderWidth = 1.0}) =>
      BoxDecoration(
        border: isBorder ? Border.all(color: kPrimaryColor, width: borderWidth) : null,
        color: color,
        borderRadius: BorderRadius.all(Radius.circular(radius)),
//    boxShadow: [
//      new BoxShadow(
//          spreadRadius: 0.08,
//          offset: Offset(0.0, 0.0), color: Colors.black.withOpacity(0.08), blurRadius: 4)
//    ],
      );

  static Decoration nonShadowDecoration({Color color = Colors.transparent, double borderSize = kDouble_1}) =>
      BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(4.0)),
        border: Border.all(color: kGreyLineColor, width: borderSize),
        color: color,
      );

  static Decoration containerDecoration({double radius = 4.0, Color color = kWhiteColor, double borderSize = kDouble_1, Color borderColor = kWhiteColor}) =>
      BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(radius)),
        border: Border.all(color: borderColor, width: borderSize),
        color: color,
      );

  static Decoration containerShadowDecoration({double radius = 4.0, Color color = kWhiteColor, double borderSize = kDouble_1, Color borderColor = kWhiteColor}) =>
      BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(radius)),
        border: Border.all(color: borderColor, width: borderSize),
        color: color,
        boxShadow: [
          new BoxShadow(
              spreadRadius: 2.1, offset: Offset(0.1, 0.1), color: Colors.black.withOpacity(0.08), blurRadius: 8)
        ],
      );

  static Decoration containerShadowRelationshipDecoration({double radius = 4.0, Color color = kWhiteColor, double borderSize = kDouble_1, Color borderColor = kWhiteColor}) =>
      BoxDecoration(
//        borderRadius: BorderRadius.all(Radius.circular(radius)),
        border: Border.all(color: borderColor, width: borderSize),
        color: color,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(radius),
          topRight: Radius.circular(radius),
        ),
//        color: color,
        boxShadow: [
          new BoxShadow(
              spreadRadius: 2.1, offset: Offset(0.1, 0.1), color: Colors.black.withOpacity(0.08), blurRadius: 8)
        ],
      );


  static shapeRoundedButton({double radius = 20.0, Color borderColor = kPrimaryColor, double borderSize = kDouble_1}) {
    return RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(radius), side: BorderSide(width: borderSize, color: borderColor));
  }

  static shapeShadowRoundedButton({Color color = kPrimaryColor, double radius = 20.0, Color borderColor = kPrimaryColor, double borderSize = kDouble_1, Color shadowColor = kPrimaryColor}) {
    return ShapeDecoration(
      color: color,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(radius)),
        side: BorderSide(color: borderColor, width: borderSize),
      ),
      shadows: [
        BoxShadow(
          color: shadowColor,
          blurRadius: 2,
          offset: Offset(0.0, 2.0),
        )
      ],
    );
  }

  static Decoration filterBorder(
          {bool isBorder = false,
          Color color = kWhiteColor,
          double radius = 4.0,
          double borderWidth = 0.5,
          Color borderColor = kPrimaryColor}) =>
      BoxDecoration(
        border: isBorder ? Border.all(color: borderColor, width: borderWidth) : null,
        color: color,
        borderRadius: BorderRadius.all(Radius.circular(radius)),
//    boxShadow: [
//      new BoxShadow(
//          spreadRadius: 0.08,
//          offset: Offset(0.0, 0.0), color: Colors.black.withOpacity(0.08), blurRadius: 4)
//    ],
      );
}
