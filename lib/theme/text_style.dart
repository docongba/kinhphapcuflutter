import 'dart:ui';

import 'package:codebase/constant/ui_constant.dart';
import 'package:flutter/material.dart';

import 'colors.dart';

class TextStyleCustom {
  static const double _space004 = 0.04;

  static TextStyle dynamicTextStyle({double fontSize = kDouble_16, Color textColor = kBlackColor, FontWeight fontWeight = FontWeight.normal}) => TextStyle(
      fontSize: fontSize,
      color: textColor,
      height: 24/fontSize,
      letterSpacing: _space004,
      fontWeight: fontWeight
  );

  static TextStyle semiBold30({Color textColor = kBlackColor, double height}) => TextStyle(
//      fontFamily: FontFamilyConstant.semiBold,
    fontSize: 30.0,
    color: textColor,
    height: height,
    letterSpacing: _space004,

  );

  static TextStyle semiBold28({Color textColor = kBlackColor, double height, fontWeight = FontWeight.normal}) => TextStyle(
//      fontFamily: FontFamilyConstant.semiBold,
    fontSize: 28.0,
    color: textColor,
    height: height,
    fontWeight: fontWeight,
    letterSpacing: _space004,
  );

   static TextStyle semiBold24({Color textColor = kBlackColor, double height, FontWeight fontWeight = FontWeight.normal}) => TextStyle(
//      fontFamily: FontFamilyConstant.semiBold,
      fontSize: 24.0,
      color: textColor,
      height: height,
      letterSpacing: _space004,
     fontWeight: fontWeight
  );

  static TextStyle semiBold20({Color textColor = kBlackColor, double height, FontWeight fontWeight = FontWeight.normal}) => TextStyle(
//      fontFamily: FontFamilyConstant.semiBold,
    fontSize: 20.0,
    color: textColor,
    height: height,
    letterSpacing: _space004,
fontWeight: fontWeight
  );

  static TextStyle semiBold18({Color textColor = kBlackColor, FontWeight fontWeight = FontWeight.normal}) => TextStyle(
//      fontFamily: FontFamilyConstant.semiBold,
      fontSize: 18.0,
      color: textColor,
      letterSpacing: _space004,
    fontWeight: fontWeight
  );


  static TextStyle semiBold16({Color textColor = kBlackColor, double height = 24.0/16.0, FontWeight fontWeight = FontWeight.normal}) => TextStyle(
//      fontFamily: FontFamilyConstant.semiBold,
      fontSize: 16.0,
      color: textColor,
      height: height,
      letterSpacing: _space004,
    fontWeight: fontWeight
  );

  static TextStyle semiBold15({Color textColor = kBlackColor, double height = 24.0/15.0, FontWeight fontWeight = FontWeight.normal}) => TextStyle(
//      fontFamily: FontFamilyConstant.semiBold,
      fontSize: 15.0,
      color: textColor,
      height: height,
      letterSpacing: _space004,
      fontWeight: fontWeight
  );

  static TextStyle semiBold13({Color textColor = kBlackColor, FontWeight fontWeight = FontWeight.normal}) => TextStyle(
//      fontFamily: FontFamilyConstant.semiBold,
      fontSize: 13.0,
      color: textColor,
      letterSpacing: _space004,
      fontWeight: fontWeight
  );

  static TextStyle semiBold12({Color textColor = kBlackColor, FontWeight fontWeight = FontWeight.normal}) => TextStyle(
//      fontFamily: FontFamilyConstant.semiBold,
      fontSize: 12.0,
      color: textColor,
      letterSpacing: _space004,
      fontWeight: fontWeight
  );

  static TextStyle semiBold14({Color textColor = kBlackColor, FontWeight fontWeight = FontWeight.normal}) => TextStyle(
//      fontFamily: FontFamilyConstant.semiBold,
      fontSize: 14.0,
      color: textColor,
      letterSpacing: _space004,
    fontWeight: fontWeight
  );

  static TextStyle medium16({Color textColor = kBlackColor}) => TextStyle(
//      fontFamily: FontFamilyConstant.medium,
      fontSize: 16.0,
      color: textColor,
      letterSpacing: _space004,
  );
  static TextStyle medium14({Color textColor = kBlackColor}) => TextStyle(
//      fontFamily: FontFamilyConstant.medium,
      fontSize: 14.0,
      color: textColor,
      letterSpacing: _space004,
  );

  static TextStyle medium12({Color textColor = kBlackColor}) => TextStyle(
//      fontFamily: FontFamilyConstant.medium,
    fontSize: 12.0,
    color: textColor,
    letterSpacing: _space004,
  );
  static TextStyle customMedium({double fontSize, Color textColor = kBlackColor}) => TextStyle(
//      fontFamily: FontFamilyConstant.medium,
      fontSize: fontSize,
      color: textColor,
      letterSpacing: _space004,
  );
   static TextStyle regular16({Color textColor = kBlackColor, double height = 24.0/16.0}) => TextStyle(
//      fontFamily: FontFamilyConstant.regular,
      fontSize: 16.0,
      color: textColor,
      height: height,
      letterSpacing: _space004,
  );
   static TextStyle regular14({Color textColor = kBlackColor, double height = 20.0/14.0}) => TextStyle(
//      fontFamily: FontFamilyConstant.regular,
    fontSize: 14.0,
    color: textColor,
    height: height,
    letterSpacing: _space004,
  );

  static TextStyle regular12({Color textColor = kBlackColor, double height = 20.0/12.0}) => TextStyle(
//      fontFamily: FontFamilyConstant.regular,
    fontSize: 12.0,
    color: textColor,
    height: height,
    letterSpacing: _space004,
  );

  static TextStyle regular10({Color textColor = kBlackColor, double height = 20.0/10.0}) => TextStyle(
//      fontFamily: FontFamilyConstant.regular,
    fontSize: 10.0,
    color: textColor,
    height: height,
    letterSpacing: _space004,
  );

  static TextStyle customRegular({double fontSize, Color textColor = kBlackColor, double height = 24.0/16.0}) => TextStyle(
//      fontFamily: FontFamilyConstant.regular,
      fontSize: fontSize,
      color: textColor,
      height: height,
      letterSpacing: _space004,
  );

  static TextStyle textFieldStyle({Color textColor = kBlackColor}) => TextStyle(
//    fontFamily: FontFamilyConstant.semiBold,
      fontSize: 14.0,
      color: textColor,
      letterSpacing: _space004,
  );
}