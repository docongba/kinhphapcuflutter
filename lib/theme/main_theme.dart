import 'package:codebase/theme/text_style.dart';
import 'package:flutter/material.dart';

import 'colors.dart';

ThemeData kShrineTheme = _buildShrineTheme();

ThemeData _buildShrineTheme() {
  final ThemeData base = ThemeData(
//    fontFamily: FontFamilyConstant.regular,
    appBarTheme: AppBarTheme(
      iconTheme: IconThemeData(color: kWhiteColor),
      color: kPrimaryColor,
      brightness: Brightness.light,
      textTheme: TextTheme(
        headline6: TextStyleCustom.medium16(
          textColor: kWhiteColor,
        ),
      ),
    ),
    scaffoldBackgroundColor: Colors.white,
  );
  return base;
}
