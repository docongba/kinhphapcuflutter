import 'package:codebase/constant/ui_constant.dart';
import 'package:codebase/theme/colors.dart';
import 'package:codebase/theme/text_style.dart';
import 'package:codebase/utils/strings.dart';
import 'package:flutter/material.dart';

class GenericDialog{
  GenericDialog({
    this.title,
    this.message,
    this.negativeLabel,
    this.positiveLabel,
    this.negativeOnPressed,
    this.positiveOnPressed,
  });

  final String title;
  final String message;
  final String negativeLabel;
  final String positiveLabel;
  final Function() negativeOnPressed;
  final Function() positiveOnPressed;

  final positiveButtonStyle = TextStyleCustom.medium16(textColor: kPrimaryColor);

  final negativeButtonStyle = TextStyleCustom.medium16(textColor: kTextColor);

  final titleStyle = TextStyleCustom.semiBold24();
  final messageStyle = TextStyleCustom.regular16();
  
  Widget create(BuildContext context) {
    return AlertDialog(
      title: title == null? null: SelectableText(title, style: titleStyle,),
      content: message == null? null: Text(message, style: messageStyle,),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4.0),
      ),

      actions: negativeLabel == null? _buildOneActions(context): _buildTwoActions(context),
    );
  }

  List<Widget> _buildOneActions(BuildContext context) {
    return [
      positiveOnPressed == null ? FlatButton(
        child: Text(positiveLabel ?? AppString.btnOk, style: positiveButtonStyle,),
        onPressed: () => Navigator.of(context).pop(),
      ): FlatButton(
        child: Text(positiveLabel ?? AppString.btnOk, style: positiveButtonStyle,),
        onPressed: positiveOnPressed,
      ),
    ];
  }

  List<Widget> _buildTwoActions(BuildContext context) {
      return [
        negativeOnPressed == null ? FlatButton(
          child: Text(negativeLabel ?? AppString.btnCancel, style: negativeButtonStyle,),
          onPressed: () => Navigator.of(context).pop(),
        ): FlatButton(
          child: Text(negativeLabel ??AppString.btnCancel, style: negativeButtonStyle,),
          onPressed: () => _onPressed(context, negativeOnPressed),
        ),
        positiveOnPressed == null ? FlatButton(
          child: Text(positiveLabel ?? AppString.btnOk, style: positiveButtonStyle,),
          onPressed: () => Navigator.of(context).pop(),
        ): FlatButton(
          child: Text(positiveLabel ?? AppString.btnOk, style: positiveButtonStyle,),
          onPressed: () => _onPressed(context, positiveOnPressed),
        ),
      ];
    }


  void _onPressed(BuildContext context, Function() onPressed) {
    Navigator.of(context).pop();
    if (onPressed != null) {
      onPressed();
    }
  }

  Future<void> show(BuildContext context, {bool barrierDismissible = false}) async {
    var dialog = create(context);

    return showDialog<void>(
      context: context,
      barrierDismissible: barrierDismissible,
      builder: (BuildContext _) {
        return dialog;
      },
    );
  }
}

class AnimationGenericDialog {

  Widget widget;
  GlobalKey parentKey;
  double parentHeight ;
  double _dy;

  AnimationGenericDialog({this.widget, this.parentHeight = 0.0, this.parentKey}) {
    RenderBox _renderBox = parentKey.currentContext.findRenderObject();
    _dy = _renderBox.localToGlobal(Offset.zero).dy;
  }

  Future<void> show(BuildContext context, {bool barrierDismissible = false, }) async {
    // RenderBox renderBox = parentKey.currentContext.findRenderObject();
    showGeneralDialog(
      context: context, 
      barrierDismissible: barrierDismissible,
      barrierLabel: "LABEL",
      pageBuilder: (_, a1, a2) {
        return widget;
      },
      transitionDuration: Duration(milliseconds: 500),
      transitionBuilder: (_, a1, a2, child) {
        final curvedValue = Curves.decelerate.transform(a1.value) - 1.0;
        return Transform(
          transform: Matrix4.translationValues(
            curvedValue * -MediaQuery.of(context).size.width,
            0.0,
            0.0,
          ),
          child: Container(
            child: AnimatedContainer(
              duration: Duration(milliseconds: 300),
              constraints: BoxConstraints(
                maxHeight: 480, maxWidth: 375,
              ),
              padding: const EdgeInsets.all(24.0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(kDouble_12),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                    color: Color(0xFF222222).withAlpha(25),
                    blurRadius: kDouble_16,
                    spreadRadius: kDouble_4,
                  ),
                ],
              ),
              child: child,
            ),
            padding: EdgeInsets.only(
              top: MediaQuery.of(context).padding.top + _dy + parentHeight,
              right: 16.0
            ),
            alignment: Alignment.topRight,
          ),
        );
      }
    );
  }
}