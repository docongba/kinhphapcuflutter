import 'dart:ui';

import 'package:codebase/constant/app_constant.dart';
import 'package:codebase/theme/colors.dart';
import 'package:codebase/theme/text_style.dart';
import 'package:codebase/utils/strings.dart';
import 'package:codebase/widget/loading/chasing_dots.dart';
import 'package:flutter/material.dart';

class AnimationDialogLoading extends StatelessWidget {
  final bool showLoading;
  final String message;
  final bool needGrayBackground;

  const AnimationDialogLoading({Key key, this.showLoading = false, this.message, this.needGrayBackground = true}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AnimatedSwitcher(
      duration: loadingAnimatedDuration,
      child: showLoading
          ? _loadingWidget(context)
          : Container(
              key: UniqueKey(),
            ),
    );
  }

  Widget _loadingWidget(BuildContext context) {
    return Center(
      key: UniqueKey(),
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: needGrayBackground? Colors.grey.withOpacity(0.35): Colors.transparent,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        child: message.isNotEmpty? _loadUIWithMessage(context): _loadUIWithoutMessage(),
    ));
  }

  _loadUIWithMessage(BuildContext context){
    return Container(
      height: kDouble_230,
      width: MediaQuery.of(context).size.width - kDouble_100,

      decoration: BoxDecoration(
        color: Colors.grey[100],
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SpinKitChasingDots(
            color: kPrimaryColor,
            size: 50,
          ),
          Text(
            AppString.titlePleaseWait,
            style: TextStyleCustom.regular16(),
          ),
          Container(
            padding: EdgeInsets.all(kDouble_16),
            child: Text(
              message,
              textAlign: TextAlign.center,
              style: TextStyleCustom.regular16(textColor: kPrimaryColor),
            ),
          )
        ],
      ),
    );
  }

  _loadUIWithoutMessage(){
    return Container(
      height: 100,
      width: 100,

      decoration: BoxDecoration(
        color: Colors.grey[100],
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SpinKitChasingDots(
            color: kPrimaryColor,
            size: 50,
          ),
          Text(
            AppString.titlePleaseWait,
            style: TextStyleCustom.regular16(),
          ),
        ],
      ),
    );
  }
}
