//import 'package:flutter/material.dart';
//
//import '../custom/input_form.dart';
//
//class AnimationResizeSearchText extends StatelessWidget {
//  final Widget secondWidget;
//  final TextEditingController searchController;
//  final FocusNode searchFocus;
//  final TickerProviderStateMixin ticker;
//  final bool enableSearching;
//  final Function(String) onChange;
//  final VoidCallback onHideSearch;
//  final bool showLeading;
//
//  const AnimationResizeSearchText({
//    Key key,
//    this.secondWidget,
//    this.ticker,
//    this.enableSearching,
//    this.onChange,
//    this.onHideSearch,
//    this.searchController,
//    this.searchFocus, this.showLeading = true,
//  }) : super(key: key);
//  @override
//  Widget build(BuildContext context) {
//    return AnimatedSize(
//      vsync: ticker,
//      duration: Duration(milliseconds: 200),
//      curve: Curves.fastOutSlowIn,
//      alignment: Alignment.centerLeft,
//      child: enableSearching ? SearchTextField(
//        controller: searchController,
//        focusNode: searchFocus,
//        onPressBack: onHideSearch,
//        showLeading: showLeading,
//        onChange: (value) => onChange(value.trim()),
//      ) : secondWidget,
//    );
//  }
//}
