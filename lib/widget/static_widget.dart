
import 'package:codebase/theme/colors.dart';
import 'package:flutter/material.dart';

Widget emptyWidget() {
  return Container();
}

Container gradientAppBar() {
  return Container(
    decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topLeft, end: Alignment.bottomRight, colors: <Color>[kStartGradient, kEndGradient])),
  );
}

