import 'package:flutter/material.dart';

class CustomLoadingWidget extends StatelessWidget {
  final String loadingMessage;

  final VoidCallback onPressed;

  const CustomLoadingWidget({Key key, this.loadingMessage, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            loadingMessage != null ? loadingMessage : "WATING FOR DATA",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.lightGreen,
              fontSize: 24,
            ),
          ),
          SizedBox(height: 24),
          CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(Colors.lightGreen),
          ),
        ],
      ),
    );
  }
}
