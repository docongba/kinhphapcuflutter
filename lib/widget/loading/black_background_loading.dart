import 'package:codebase/constant/ui_constant.dart';
import 'package:codebase/theme/colors.dart';
import 'package:codebase/widget/loading/chasing_dots.dart';
import 'package:flutter/material.dart';


class BlackBackgroundLoadingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        new Opacity(
          opacity: 0.6,
          child: const ModalBarrier(dismissible: false, color: Colors.grey),
        ),
        SpinKitChasingDots(
          color: kGreenColor,
          size: kDouble_50,
        )
      ],
    );
  }
}
