import 'package:codebase/constant/ui_constant.dart';
import 'package:codebase/theme/colors.dart';
import 'package:codebase/widget/loading/chasing_dots.dart';
import 'package:flutter/material.dart';

class SingleLoadingWdiget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        color: Colors.transparent,
        child: SpinKitChasingDots(color: kGreenColor, size: kDouble_50),
        );
  }
}
