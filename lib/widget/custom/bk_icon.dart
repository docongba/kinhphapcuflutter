import 'package:codebase/constant/ui_constant.dart';
import 'package:codebase/theme/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
class BKIcon extends StatelessWidget{

  final double width;
  final double height;
  final Color color;
  final String resource;


  BKIcon({this.width = kDouble_24, this.height = kDouble_24, this.color = kPrimaryColor, this.resource});

  @override
  Widget build(BuildContext context) {

    if(this.color == Colors.transparent){
      return SvgPicture.asset(
        resource,
        width: width,
        height: height,
      );
    }
      return SvgPicture.asset(
        resource,
        color: this.color,
        width: width,
        height: height,
      );
  }
}