import 'package:flutter/material.dart';

class CustomEmptyWidget extends StatelessWidget {
  final String emptyMessage;

  final VoidCallback onPressed;

  const CustomEmptyWidget({Key key, this.emptyMessage, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            emptyMessage != null ? emptyMessage : "EMPTY",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.lightGreen,
              fontSize: 24,
            ),
          ),
          SizedBox(height: 24),
          CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(Colors.lightGreen),
          ),
        ],
      ),
    );
  }
}
