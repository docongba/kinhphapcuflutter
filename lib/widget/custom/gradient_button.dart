import 'package:codebase/constant/ui_constant.dart';
import 'package:codebase/theme/colors.dart';
import 'package:codebase/theme/custom_decoration.dart';
import 'package:codebase/theme/text_style.dart';
import 'package:codebase/widget/custom/bk_icon.dart';
import 'package:codebase/widget/static_widget.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class GradientButton extends StatelessWidget {
  final VoidCallback onTap;
  final bool active;
  final double width;
  final double height;
  final String title;
  final String assetIcon;
  final Color assetColor;
  final Color buttonColor;
  final double buttonRadius;
  final Color titleColor;
  final double titleSize;
  final double padding;

  GradientButton(
      {this.assetColor = Colors.transparent,
      this.padding = 0.0,
      this.titleSize = kDouble_16,
      this.titleColor = kWhiteColor,
      this.buttonRadius = kBtnRadius,
      this.onTap,
      this.active = true,
      this.width = 0,
      this.title = '',
      this.assetIcon = '',
      this.buttonColor = kPrimaryColor,
      this.height = kButtonHeight});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width > 0 ? width : MediaQuery.of(context).size.width - kDouble_100,
      decoration: new BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(buttonRadius)),
        gradient: new LinearGradient(
          colors: [kStartGradient, kEndGradient],
          begin: FractionalOffset.centerLeft,
          end: FractionalOffset.centerRight,
        ),
      ),
      child: FlatButton(
          shape: CustomDecoration.shapeRoundedButton(radius: buttonRadius, borderColor: Colors.transparent),
          onPressed: active? this.onTap: (){

          },
          height: height,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              assetIcon.isEmpty
                  ? emptyWidget()
                  : BKIcon(
                      resource: assetIcon,
                      color: assetColor,
                    ),
              SizedBox(
                width: assetIcon.isEmpty ? 0.0 : kDouble_8,
              ),
              title.isEmpty
                  ? emptyWidget()
                  : Text(
                      title,
                      style: TextStyleCustom.dynamicTextStyle(fontSize: titleSize, textColor: titleColor),
                      textAlign: TextAlign.center,
                    ),
            ],
          )),
    );
  }
}
