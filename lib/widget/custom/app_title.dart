import 'dart:ui';

import 'package:codebase/theme/colors.dart';
import 'package:codebase/theme/text_style.dart';
import 'package:flutter/material.dart';

class AppTitle extends StatelessWidget {
  final String title;

  const AppTitle({
      this.title,});
  @override
  Widget build(BuildContext context) {
    return Text(title, style: TextStyleCustom.semiBold18(fontWeight: FontWeight.w500, textColor: kWhiteColor),);
  }
}


