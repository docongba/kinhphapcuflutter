import 'package:codebase/constant/ui_constant.dart';
import 'package:codebase/manager/route/app_navigator.dart';
import 'package:codebase/theme/colors.dart';
import 'package:codebase/theme/custom_decoration.dart';
import 'package:codebase/theme/text_style.dart';
import 'package:codebase/utils/strings.dart';
import 'package:flutter/material.dart';
class CancelButton extends StatelessWidget {
  final VoidCallback onTap;
  final double width;
  final String title;

  CancelButton({this.onTap, this.width = 0.0, this.title = ''});

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: onTap == null? {AppNavigator().pop()}: onTap,
      color: kWhiteColor,
      shape: CustomDecoration.shapeRoundedButton(radius: kDouble_16, borderColor: kWhiteColor),
      child: Container(
          width: width > 0? width: MediaQuery.of(context).size.width - kDouble_100,
          alignment: Alignment.center,
          height: kButtonHeight,
          child: Text(
            title.isEmpty? AppString.btnCancel: title,
            style: TextStyleCustom.semiBold16(textColor: kPrimaryColor),
            textAlign: TextAlign.center,
          )),
    );
  }
}