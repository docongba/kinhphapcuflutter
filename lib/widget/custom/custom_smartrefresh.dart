//import 'package:flutter/cupertino.dart';
//import 'package:flutter/material.dart';
//import 'package:pull_to_refresh/pull_to_refresh.dart';
//
//class CustomSmartRefresher extends StatefulWidget {
//  final RefreshController refreshController;
//  final bool enablePullUp;
//  final CustomFooter customFooter;
//  final VoidCallback onLoading;
//  final VoidCallback onRefresh;
//  final Widget child;
//
//  const CustomSmartRefresher(
//
//    {
//    Key key,
//    this.refreshController,
//    this.onLoading,
//    this.onRefresh,
//    this.child,
//    this.customFooter,
//    this.enablePullUp = true,
//  }) :assert(refreshController != null),
//      assert(child != null),
//      assert(onRefresh != null),
//   super(key: key);
//  @override
//  _CustomSmartRefresherState createState() => _CustomSmartRefresherState();
//}
//
//class _CustomSmartRefresherState extends State<CustomSmartRefresher> {
//
// CustomFooter _customFooter;
//
// @override
//  void initState() {
//    super.initState();
//    setCustomFooter();
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return SmartRefresher(
//      controller: this.widget.refreshController,
//      enablePullUp: this.widget.enablePullUp,
//      header: MaterialClassicHeader(),
//      footer: this._customFooter,
//      onRefresh: this.widget.onRefresh,
//      onLoading: this.widget.onLoading,
//      child: this.widget.child,
//    );
//  }
//
//  void setCustomFooter(){
//    if (this.widget.customFooter == null){
//      this._customFooter = CustomFooter(
//        builder: (BuildContext context, LoadStatus mode) {
//          Widget body;
//          if (mode == LoadStatus.loading) {
//            body = CupertinoActivityIndicator();
//          } else {
//            body = Text("");
//          }
//          return Container(
//            height: 55.0,
//            child: Center(child: body),
//          );
//        },
//      );
//    } else {
//      this._customFooter = this.widget.customFooter;
//    }
//  }
//}
