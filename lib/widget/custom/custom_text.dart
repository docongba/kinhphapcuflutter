import 'package:codebase/constant/properties_constant.dart';
import 'package:codebase/constant/ui_constant.dart';
import 'package:codebase/theme/colors.dart';
import 'package:codebase/theme/custom_decoration.dart';
import 'package:codebase/theme/text_style.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class LabelText extends StatelessWidget {
  /// - `medium 16`

  final String label;
  final Color color;

  const LabelText(this.label, {Key key, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      label,
      style: TextStyleCustom.medium14(textColor: color),
    );
  }
}

class BigLabelText extends StatelessWidget {
  /// - `medium 16`

  final String label;
  final Color color;

  const BigLabelText(this.label, {Key key, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      label,
      style: TextStyleCustom.medium16(textColor: color),
    );
  }
}

class TitleText extends StatelessWidget {
  /// - `semiBold 16`

  final String label;
  final Color color;

  const TitleText(this.label, {Key key, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      label,
      style: TextStyleCustom.semiBold16(textColor: color, height: 24.0 / 16.0),
    );
  }
}

class HeaderText extends StatelessWidget {
  /// - `semiBold 18`

  final String label;
  final Color color;

  const HeaderText(this.label, {Key key, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      label,
      style: TextStyleCustom.semiBold18(textColor: color),
    );
  }
}

class BiggestTitleText extends StatelessWidget {
  /// - `semiBold 24`

  final String label;
  final Color color;
  final double height;

  const BiggestTitleText(this.label, {Key key, this.color, this.height}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      label,
      style: TextStyleCustom.semiBold24(
        textColor: color,
        height: height,
      ),
    );
  }
}

class DateTimeText extends StatelessWidget {
  final String label;
  final Color color;

  const DateTimeText(this.label, {Key key, this.color = kTextColor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      label,
      style: TextStyleCustom.regular14(textColor: color),
    );
  }
}

class DescriptionText extends StatelessWidget {
  final String label;
  final Color color;
  final int maxLine;

  const DescriptionText(this.label, {Key key, this.color = kBlackColor, this.maxLine}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      label,
      style: TextStyleCustom.regular14(textColor: color, height: 20.0 / 14.0),
      maxLines: maxLine,
      overflow: TextOverflow.ellipsis,
    );
  }
}

class BoxBackGroundContent extends StatelessWidget {
  final String label;
  final double width;

  const BoxBackGroundContent({Key key, this.label, this.width}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      padding: const EdgeInsets.symmetric(vertical: kDouble_12, horizontal: kDouble_16),
      decoration: CustomDecoration.cardDecoration(
        color: kPrimaryColor,
      ),
      alignment: Alignment.center,
      child: Text(
        label,
        style: TextStyleCustom.regular14(
          textColor: kWhiteColor,
          height: 20.0/14.0
        ),
      ),
    );
  }
}

class BoxContent extends StatelessWidget {
  final dynamic label;
  final double width;
  final bool isDateTime;

  const BoxContent({Key key, this.label, this.width, this.isDateTime = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      padding: const EdgeInsets.symmetric(vertical: kDouble_12, horizontal: kDouble_16),
      decoration: CustomDecoration.nonShadowDecoration(),
      alignment: Alignment.centerLeft,
      child: Text(
        !isDateTime ? label.toString() : DateFormat(DateTimeFormat.DD_MM_YYYY).format(label),
        style: TextStyleCustom.regular14(height: 20.0/14.0),
      ),
    );
  }
}

class CustomTextField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return TextField(
      style: TextStyleCustom.textFieldStyle(),
    );
  }
}
