import 'dart:ui';

import 'package:codebase/constant/ui_constant.dart';
import 'package:codebase/theme/text_style.dart';
import 'package:flutter/material.dart';
class NemoDialog{
  NemoDialog({
    this.title,
    this.body,
  });
  final String title;
  final Widget body;

  final titleStyle = TextStyleCustom.semiBold20();

  Widget create(BuildContext context) {

    return AlertDialog(
      contentPadding: EdgeInsets.all(8.0),
//      titlePadding: EdgeInsets.all(kDouble_16),
      insetPadding: EdgeInsets.all(kDouble_20),
      title: title.isEmpty? null: Text(

        title, style: titleStyle, textAlign: TextAlign.center,),
      content: Container(
        width: MediaQuery.of(context).size.width,
          child: body),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16.0),
      ),

//      actions: negativeLabel == null? _buildOneActions(context): _buildTwoActions(context),
    );
  }


  Future<void> show(BuildContext context, {bool barrierDismissible = false}) async {
    var dialog = create(context);

    return showDialog<void>(
      context: context,
      barrierDismissible: barrierDismissible,
      builder: (BuildContext _) {
        return dialog;
      },
    );
  }
}