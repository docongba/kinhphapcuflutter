import 'package:codebase/constant/assets_constant.dart';
import 'package:codebase/constant/ui_constant.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/svg.dart';


class ShowPasswordIcon extends StatelessWidget {
  final VoidCallback onPressed;
  final bool isShow;

  const ShowPasswordIcon({Key key, this.onPressed, this.isShow = false}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      borderRadius: BorderRadius.all(
        Radius.circular(kDouble_8),
      ),
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: SvgPicture.asset(
          isShow ? AssetsConstant.ic_hide_pass : AssetsConstant.ic_show_pass,
          height: kDouble_16,
          width: kDouble_16,
        ),
      ),
    );
  }
}

class RightArrowIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(
      AssetsConstant.ic_right_arrow,
    );
  }
}

class LeftArrowIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return RotatedBox(
      quarterTurns: -2,
      child: SvgPicture.asset(
        AssetsConstant.ic_right_arrow,
      ),
    );
  }
}

class UpArrowIcon extends StatelessWidget {
  final double width;
  final double height;

  const UpArrowIcon({Key key, this.width, this.height}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return RotatedBox(
      quarterTurns: -1,
      child: SvgPicture.asset(
        AssetsConstant.ic_right_arrow,
        height: height, width: width,
      ),
    );
  }
}

class DownArrowIcon extends StatelessWidget {
  final double width;
  final double height;

  const DownArrowIcon({Key key, this.width, this.height}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return RotatedBox(
      quarterTurns: 1,
      child: SvgPicture.asset(
        AssetsConstant.ic_right_arrow,
      ),
    );
  }
}

class SearchIcon extends StatelessWidget {
  final VoidCallback onPressed;

  const SearchIcon({Key key, this.onPressed}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: SvgPicture.asset(
        AssetsConstant.ic_search,
      ),
    );
  }
}

class FilterIcon extends StatelessWidget {
  final VoidCallback onPressed;

  const FilterIcon({Key key, this.onPressed}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: SvgPicture.asset(
        AssetsConstant.ic_filter,
        width: 24,
        height: 24,
      ),
    );
  }
}
