import 'package:codebase/constant/ui_constant.dart';
import 'package:codebase/theme/colors.dart';
import 'package:codebase/theme/custom_decoration.dart';
import 'package:codebase/theme/text_style.dart';
import 'package:codebase/widget/custom/bk_icon.dart';
import 'package:codebase/widget/static_widget.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
class BkButton extends StatelessWidget {
  final VoidCallback onTap;
  final bool active;
  final double width;
  final double height;
  final String title;
  final String assetIcon;
  final Color assetColor;
  final Color buttonColor;
  final Color buttonBorderColor;
  final double buttonRadius;
  final Color titleColor;
  final double titleSize;
  final double padding;

  BkButton({this.assetColor = Colors.transparent, this.padding = 0.0, this.titleSize = kDouble_16, this.titleColor = kWhiteColor, this.buttonBorderColor = kPrimaryColor, this.buttonRadius = kBtnRadius, this.onTap, this.active = true, this.width = 0, this.title = '', this.assetIcon = '', this.buttonColor = kPrimaryColor, this.height = kButtonHeight});

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      padding: EdgeInsets.symmetric(horizontal: padding),
      onPressed: active? this.onTap: (){

      },
      color: active? buttonColor: kGreyTextColor,
      height: height,
      shape: CustomDecoration.shapeRoundedButton(radius: buttonRadius, borderColor: buttonBorderColor),
      child: Container(
          width: width > 0? width: MediaQuery.of(context).size.width - kDouble_100,
          alignment: Alignment.center,
          height: height,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              assetIcon.isEmpty? emptyWidget(): BKIcon(resource: assetIcon, color: assetColor,),
              SizedBox(
                width: assetIcon.isEmpty? 0.0: kDouble_8,
              ),
              title.isEmpty? emptyWidget(): Text(
                title,
                style: TextStyleCustom.dynamicTextStyle(fontSize: titleSize, textColor: titleColor),
                textAlign: TextAlign.center,
              ),
            ],
          )),
    );
  }
}