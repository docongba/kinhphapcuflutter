import 'package:codebase/constant/enum_constant.dart';
import 'package:codebase/widget/loading/custom_loading.dart';
import 'package:flutter/material.dart';

import 'custom_empty.dart';
import 'custom_error.dart';

class CustomStreamBuilder<T> extends StatelessWidget {

  ///  IF error/empty/wating widget is null, widget will show [CustomErrorWidget]/[CustomEmptyWidget]/[CustomLoadingWidget]. 
  ///  So we can add [defaultMessage] & [defaultAction] for [CustomEmptyWidget]/[CustomLoadingWidget] or they will get default [mobileLabel] & [actions]
  ///  IF [errorWidget] is [null], [CustomErrorWidget] will get error message from snapshot

  final Stream stream;
  final Widget Function(BuildContext context, AsyncSnapshot<T> snapshot) waitingWidget;
  final String defaultWatingMessage;
  final VoidCallback defaultWatingAction;
  final Widget Function(BuildContext context, AsyncSnapshot<T> snapshot) errorWidget;
  final VoidCallback defaultErrorAcction;
  final Widget Function(BuildContext context, AsyncSnapshot<T> snapshot) dataWidget;
  final Widget emptyWidget;
  final String defaultEmptyMessage;
  final VoidCallback defaultEmptyAction;
  final T initialdata;

  const CustomStreamBuilder({
    Key key,
    this.stream, 
    this.waitingWidget, 
    this.dataWidget, 
    this.emptyWidget, 
    this.errorWidget, 
    this.defaultWatingMessage, 
    this.defaultWatingAction, 
    this.defaultErrorAcction, 
    this.defaultEmptyMessage, 
    this.defaultEmptyAction, this.initialdata, 
  }) : super(key: key);

  

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<T>(
      stream:  stream,
      initialData: initialdata,
      builder: (context,snapshot)  {
        print("------------------- rebuild stream   ");
        print(snapshot.connectionState);
        
        if (snapshot.hasError ){
          if (snapshot.error == StreamStatus.loading){
            // * ACTIVE LOADING STATUS FOR STREAM.
            return waitingWidget != null ? waitingWidget(context, snapshot) : CustomLoadingWidget(loadingMessage: defaultWatingMessage, onPressed: defaultWatingAction,);
          } else {
            // * ON ERROR
            WidgetsBinding.instance.addPostFrameCallback((_) => normalDialog(context));
            return errorWidget != null ? errorWidget : CustomErrorWidget(errorMessage: snapshot.error.toString(),onRetryPressed: defaultErrorAcction,);
          }
        } else if (snapshot.connectionState == ConnectionState.waiting){
          // * ON LOADING
          return waitingWidget != null ? waitingWidget(context, snapshot) : CustomLoadingWidget(loadingMessage: defaultWatingMessage, onPressed: defaultWatingAction,);
        } else if (snapshot.connectionState == ConnectionState.active && snapshot.hasData){
          if (snapshot.data is List && (snapshot.data as List).isEmpty){
            // * ON  HASDATA  BUT  EMPTY
            return emptyWidget != null ? emptyWidget : CustomEmptyWidget(emptyMessage: defaultEmptyMessage, onPressed: defaultEmptyAction,);
          } else {
            // * HAVE DATA
            return dataWidget(context, snapshot);
          }
        } else {
          // * EMPTY
          return emptyWidget != null ? emptyWidget : CustomEmptyWidget(emptyMessage: defaultEmptyMessage, onPressed: defaultEmptyAction,);
        }
      },
    );
  }

  Future<Widget> normalDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("TITLE"),
            content: Text("CONTENT"),
          );
        });
  }
}