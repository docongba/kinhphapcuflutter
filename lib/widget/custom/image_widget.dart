import 'package:codebase/constant/assets_constant.dart';
import 'package:codebase/constant/ui_constant.dart';
import 'package:codebase/theme/colors.dart';
import 'package:flutter/material.dart';

class AvatarWidget extends StatelessWidget {
  final String url;
  final VoidCallback onPressed;
  final double height;
  final double width;
  final Color borderColor;
  final double borderSize;

  const AvatarWidget({Key key, this.url, this.onPressed, this.height, this.width, this.borderSize = 1, this.borderColor = kWhiteColor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: this.height?? kDouble_72,
        width: this.width?? kDouble_72,
        decoration: BoxDecoration(
          color: kBackgroundColor,
          shape: BoxShape.circle,
          border: Border.all(
            width: borderSize, //
            color: borderColor//                <--- border width here
          ),
        ),
        child: ClipOval(
          child: FadeInImage.assetNetwork(
            placeholder: AssetsConstant.place_holder_avatar,
            image:url,
          ),
        )
    );
  }
}

class LoadNetworkImage extends StatelessWidget {
  final String url;
  final double height;
  final double width;

  const LoadNetworkImage({Key key, this.url = '', this.height, this.width}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FadeInImage.memoryNetwork(
//      placeholder: kTransparentImage,
      image: url,
      height: height,
      width: width,
      imageErrorBuilder: (_, __, ___) {
        return ImagePlaceHolderWidget(height: height, width: width);
      },
    );
  }
}

class ImagePlaceHolderWidget extends StatelessWidget {
  final double height;
  final double width;

  const ImagePlaceHolderWidget({Key key, this.height, this.width}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
        color: kBackgroundColor,
        borderRadius: borderRadiusAll,
      ),
    );
  }
}
