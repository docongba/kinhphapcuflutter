import 'package:codebase/constant/ui_constant.dart';
import 'package:codebase/core/model/drawer_menu_item.dart';
import 'package:codebase/theme/colors.dart';
import 'package:codebase/theme/text_style.dart';
import 'package:codebase/widget/custom/image_widget.dart';
import 'package:flutter/material.dart';

class DrawerMenuWidget extends StatelessWidget {
  DrawerMenuWidget({
    Key key,
    this.imageUrl,
    this.accountName,
    this.heightBottomNavigationBar,
    this.drawerItems,
    this.onSelected,
  }) : super(key: key);

  final String imageUrl;
  final String accountName;
  final double heightBottomNavigationBar;
  final List<DrawerMenuItem> drawerItems;
  final Function(String) onSelected;

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: ListView(
          children: <Widget>[
            _DrawerMenuHeader(
              imageUrl: imageUrl,
              accountName: accountName,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                vertical: kDouble_14,
              ),
              child: new Column(
                children: drawerItems.map((item) {
                  return _BuildMenuItem(
                    onSelected: onSelected,
                    item: item,
                  );
                }).toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _DrawerMenuHeader extends StatelessWidget {
  const _DrawerMenuHeader({
    Key key,
    @required this.imageUrl,
    @required this.accountName,
  }) : super(key: key);

  final String imageUrl;
  final String accountName;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(
        left: kDouble_24,
        right: kDouble_24,
        top: kDouble_24,
        bottom: kDouble_14,
      ),
      decoration: BoxDecoration(
        color: kPrimaryColor,
        border: Border(
          bottom: BorderSide(color: Colors.grey.withOpacity(0.5), width: 0.5),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          AvatarWidget(url: imageUrl, width: kDouble_72, height: kDouble_72, onPressed: (){
          },),

          Container(
            margin: EdgeInsets.only(top: kDouble_12),
            child: Text(accountName, style: TextStyleCustom.medium16(textColor: kWhiteColor),),
          )
        ],
      ),
    );
  }
}

class _BuildMenuItem extends StatelessWidget {
  const _BuildMenuItem({
    Key key,
    this.item,
    @required this.onSelected,
  }) : super(key: key);

  final Function(String) onSelected;
  final DrawerMenuItem item;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => onSelected(item.key),
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: kDouble_14,
          horizontal: kDouble_24,
        ),
        child: Row(
          children: <Widget>[
            Flexible(
              flex: 0,
              child: Container(
                width: kDouble_24,
                height: kDouble_24,
                child: Image.asset(item.icon, width: 24.0, height: 24.0, color: kTextColor,),
              ),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(
                  left: kDouble_20,
                ),
                child: Text(
                  item.title,
                  style: TextStyle(
                    color: kMenuItemColor,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
