//import 'package:bk_package/constant/assets_constant.dart';
//import 'package:bk_package/constant/properties_constant.dart';
//import 'package:bk_package/constant/ui_constant.dart';
//import 'package:bk_package/manager/route/app_navigator.dart';
//import 'package:bk_package/theme/colors.dart';
//import 'package:bk_package/theme/custom_decoration.dart';
//import 'package:bk_package/theme/text_style.dart';
//import 'package:bk_package/theme/textfield_style.dart';
//import 'package:bk_package/utils/action_delay.dart';
//import 'package:bk_package/utils/strings.dart';
//import 'package:bk_package/widget/custom/bk_icon.dart';
//import 'package:bk_package/widget/custom/custom_dialog.dart';
//import 'package:bk_package/widget/custom/custom_text.dart';
//import 'package:bk_package/widget/custom/icons_button.dart';
//import 'package:flutter/material.dart';
//import 'package:flutter_svg/flutter_svg.dart';
//import 'package:intl/intl.dart';
//
//import '../generic_dialog.dart';
//
//class InputInformationItem extends StatelessWidget {
//  const InputInformationItem({
//    Key key,
//    this.context,
//    this.controller,
//    this.title,
//    this.onTap,
//    this.enable = true,
//    this.titleColor = kBlackColor,
//    this.contentColor = kBlackColor,
//    this.minLine = 1,
//    this.isNumber = false,
//    this.maxLine = 1,
//    this.paddingLeft,
//    this.paddingRight,
//    this.focusNode,
//    this.onSubmitted,
//    this.hint = '',
//    this.onChanged,
//  }) : super(key: key);
//
//  final BuildContext context;
//  final TextEditingController controller;
//  final String title;
//  final VoidCallback onTap;
//  final bool enable;
//  final String hint;
//  final Color titleColor;
//  final Color contentColor;
//  final int minLine;
//  final bool isNumber;
//  final int maxLine;
//  final double paddingLeft;
//  final double paddingRight;
//  final FocusNode focusNode;
//  final Function(String) onSubmitted;
//  final Function(String) onChanged;
//
//  @override
//  Widget build(BuildContext context) {
//    return InkWell(
//      onTap: onTap,
//      child: Padding(
//        padding: EdgeInsets.only(
//          left: paddingLeft ?? kDouble_24,
//          right: paddingRight ?? kDouble_12,
//        ),
//        child: Row(
//          children: <Widget>[
//            LabelText(
//              title,
//              color: titleColor,
//            ),
//            Flexible(
//              child: Container(
//                padding: EdgeInsets.only(left: kDouble_16),
//                child: TextFormField(
//                  controller: controller,
//                  focusNode: focusNode,
//                  onFieldSubmitted: onSubmitted,
//                  onChanged: onChanged,
//                  keyboardType: isNumber ? TextInputType.number : TextInputType.text,
//                  minLines: minLine,
//                  maxLines: maxLine,
//                  enabled: enable,
//                  textAlign: TextAlign.end,
//                  style: TextStyleCustom.medium14(textColor: contentColor),
//                  decoration: TextFieldUtils().unBorderTextFormFieldDecoration(hint: hint),
//                ),
//              ),
//            ),
//          ],
//        ),
//      ),
//    );
//  }
//}
//
//class SelectDateForm extends StatelessWidget {
//  final TextEditingController controller;
//  final String title;
//  final Function(DateTime) result;
//  final EdgeInsets contentPadding;
//  final double paddingLeft;
//  final double paddingRight;
//  final Color contentColor;
//  final Color titleColor;
//  final String hint;
//
//  const SelectDateForm({Key key, this.controller, this.title, this.result, this.contentPadding, this.paddingLeft, this.paddingRight, this.contentColor, this.titleColor, this.hint}) : super(key: key);
//
//  @override
//  Widget build(BuildContext context) {
//    return InkWell(
//      onTap: () => selectDate(context),
//      child: Padding(
//        padding: EdgeInsets.only(
//          left: paddingLeft ?? kDouble_24,
//          right: paddingRight ?? kDouble_12,
//        ),
//        child: Row(
//          children: [
//            LabelText(
//              title,
//              color: titleColor,
//            ),
//            Flexible(
//              child: TextField(
//                controller: controller,
//                enabled: false,
//                textAlign: TextAlign.right,
//                style: TextStyleCustom.medium14(textColor: contentColor),
//                decoration: TextFieldUtils().unBorderTextFormFieldDecoration(hint: hint),
//              ),
//            )
//          ],
//        ),
//      ),
//    );
//  }
//
//  void selectDate(BuildContext context) {
//    CustomDialog().selectDate(context, (value) {
//      // CLOSE DIALOg
//      AppNavigator().pop();
//      result(value);
//    });
//  }
//}
//
//class CustomDivider extends StatelessWidget {
//  final double indent;
//  final double height;
//  final double thickness;
//
//  const CustomDivider({Key key, this.indent = kDouble_24, this.height = kDoubleZero, this.thickness}) : super(key: key);
//  @override
//  Widget build(BuildContext context) {
//    return Divider(
//      indent: indent,
//      height: height,
//      thickness: thickness,
//    );
//  }
//}
//
//class SearchTextField extends StatelessWidget {
//  final TextEditingController controller;
//  final FocusNode focusNode;
//  final VoidCallback onPressBack;
//  final Function(String) onChange;
//  final bool showLeading;
//
//  const SearchTextField({
//    Key key,
//    this.controller,
//    this.focusNode,
//    this.onPressBack,
//    this.onChange,
//    this.showLeading = true,
//  }) : super(key: key);
//
//  @override
//  Widget build(BuildContext context) {
//    return TextField(
//      controller: controller,
//      focusNode: focusNode,
//      style: TextStyleCustom.textFieldStyle(textColor: kWhiteColor),
//      onChanged: (value) => Debouncer(milliseconds: 200).run(() {
//        onChange(value);
//      }),
//      decoration: TextFieldUtils().unBorderTextFormFieldDecoration(
//          color: kPrimaryColor,
//          icon: showLeading
//              ? InkWell(
//                  onTap: () {
//                    focusNode.unfocus();
//                    controller.clear();
//                    onPressBack();
//                  },
//                  child: Container(
//                    padding: const EdgeInsets.all(5),
//                    child: BKIcon(resource: AssetsConstant.ic_back, color: kGreyTextColor,)
//
//                  ),
//                )
//              : null,
//          hint: AppString.labelSearch,
//          suffixIcon: Container(
//            padding: const EdgeInsets.all(kDouble_12),
//            height: kDouble_24,
//            width: kDouble_24,
//            child: InkWell(
//              onTap: () {
//                controller.clear();
//              },
////              child: SvgPicture.asset(
//////                AssetsConstant.ic_cancel,
////                height: kDouble_24,
////                width: kDouble_24,
////              ),
//            ),
//          )),
//    );
//  }
//}
//
//class SelectDateTextField extends StatelessWidget {
//  final double height;
//  final AlignmentGeometry align;
//  final EdgeInsets margin;
//  final EdgeInsets padding;
//  final TextStyle textStyle;
//  final TextAlign textAlign;
//  final DateTime initDate;
//  final BoxDecoration decoration;
//  final DateTime minDate;
//  final DateTime maxDate;
//  final TextEditingController _controller = TextEditingController();
//
//  final Function(DateTime) onTap;
//
//  SelectDateTextField({
//    Key key,
//    this.height,
//    this.align,
//    this.margin,
//    this.padding,
//    this.textStyle,
//    this.textAlign,
//    this.initDate,
//    this.decoration,
//    this.minDate,
//    this.maxDate,
//    this.onTap,
//  }) : super(key: key) {
//    _controller.text = DateFormat(DateTimeFormat.DD_MM_YYYY).format(initDate);
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      height: 44.0,
//      padding: const EdgeInsets.symmetric(horizontal: kDouble_16),
//      decoration: CustomDecoration.nonShadowDecoration(),
//      child: InkWell(
//        onTap: () => _onTap(context),
//        child: Row(
//          crossAxisAlignment: CrossAxisAlignment.center,
//          children: [
//            Flexible(
//              child: TextField(
//                controller: _controller,
//                style: TextStyleCustom.regular14(),
//                decoration: TextFieldUtils().unBorderTextFormFieldDecoration(
//                    color: Colors.transparent,
//                    contentPadding: const EdgeInsets.only(
//                      left: 0.0,
//                      right: 0.0,
//                      bottom: 6.0,
//                    )),
//                enabled: false,
//              ),
//            ),
//            SvgPicture.asset(
//              AssetsConstant.ic_event,
//              color: kTextDisableColor,
//            )
//          ],
//        ),
//      ),
//    );
//  }
//
//  void _onTap(BuildContext context) {
//    CustomDialog().selectDate(
//      context,
//      (value) {
//        Navigator.of(context).pop();
//        _controller.text = DateFormat(DateTimeFormat.DD_MM_YYYY).format(value);
//        if (onTap != null) {
//          onTap(value);
//        }
//      },
//      initialDateTime: initDate,
//      minimumDate: minDate,
//      maximumDate: maxDate,
//    );
//  }
//}
//
///*
//class SelectDateTextField extends StatefulWidget {
//  final double height;
//  final AlignmentGeometry align;
//  final EdgeInsets margin;
//  final EdgeInsets padding;
//  final TextStyle textStyle;
//  final TextAlign textAlign;
//  final int maxLine;
//  final int minLine;
//  final Color cursorColor;
//  final DateTime initDate;
//  final BoxDecoration decoration;
//  final DateTime minDate;
//  final DateTime maxDate;
//
//  final Function(DateTime) onTap;
//
//  const SelectDateTextField({
//    Key key,
//    this.height = kDouble_44,
//    this.initDate,
//    this.align = Alignment.center,
//    this.margin,
//    this.padding,
//    this.textStyle,
//    this.textAlign = TextAlign.left,
//    this.maxLine = 1,
//    this.minLine = 1,
//    this.cursorColor = kPrimaryColor,
//    this.onTap,
//    this.decoration, this.minDate, this.maxDate,
//  }) : super(key: key);
//
//  @override
//  _SelectDateTextFieldState createState() => _SelectDateTextFieldState();
//}
//
//class _SelectDateTextFieldState extends State<SelectDateTextField> {
//  DateTime _initialDate;
//
//  @override
//  void initState() {
//    // widget.initDate != null && widget.initDate.isNotEmpty ? _initialDate = widget.initDate : _initialDate = "";
//    _initialDate = widget.initDate;
//    super.initState();
//  }
//
//  @override
//  void didUpdateWidget(SelectDateTextField oldWidget) {
//    super.didUpdateWidget(oldWidget);
//    _initialDate = widget.initDate;
//  }
//
//
//  Widget build(BuildContext context) {
//    return InkWell(
//      onTap: () {
//        CustomDialog().selectDate(
//          context,
//          (value) {
//            Navigator.of(context).pop();
//            setState(() {
//              _initialDate = value;
//              widget.onTap(value);
//            });
//          },
//          initialDateTime: _initialDate,
//          minimumDate: widget.minDate,
//          maximumDate: widget.maxDate
//        );
//      },
//      child: Container(
//        // decoration: shadowDecoration(),
//        height: widget.height,
//        padding: widget.padding == null
//            ? const EdgeInsets.only(
//                left: kDouble_16,
//                right: kDouble_12,
//              )
//            : widget.padding,
//        margin: widget.margin,
//        alignment: widget.align,
//        decoration: widget.decoration ??
//            BoxDecoration(
//                borderRadius: borderRadiusAll,
//                border: Border.all(
//                  color: kGreyLineColor,
//                  width: kDouble_1,
//                )),
//        child: Row(
//          mainAxisAlignment: MainAxisAlignment.spaceBetween,
//          children: <Widget>[
//            Text(
//              // widget.initDate == null || widget.initDate.isEmpty ? "" : formatToddMMyyyy(_initialDate),
//              // formatToddMMyyyy(_initialDate),
//              DateTimeUtils.toDDMMYYYY(_initialDate),
//              style: widget.textStyle ?? TextStyleCustom.regular16(),
//              maxLines: widget.maxLine,
//            ),
//            SvgPicture.asset(
//              AssetsConstant.ic_event,
//              color: kTextDisableColor,
//            )
//          ],
//        ),
//      ),
//    );
//  }
//}
//*/
//
//class SelectNumberTextField extends StatelessWidget {
//  final TextEditingController _controller = TextEditingController();
//  final FocusNode _focusNode = FocusNode();
//  final bool canNegative;
//  final Function(int) onResult;
//
//  SelectNumberTextField({Key key, this.onResult, this.canNegative = false}) : super(key: key);
//
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      height: 44.0,
//      decoration: BoxDecoration(
//          borderRadius: borderRadiusAll,
//          border: Border.all(
//            color: kGreyLineColor,
//          )),
//      child: Row(
//        children: [
//          Flexible(
//            child: TextField(
//              controller: _controller,
//              focusNode: _focusNode,
//              keyboardType: TextInputType.number,
//              onChanged: (value) {
//                value = value.trim();
//                onResult(int.parse(value));
//              },
//              decoration: TextFieldUtils().unBorderTextFormFieldDecoration(
////                hint: AppString.hintSelectOtherNumber,
//              ),
//            ),
//          ),
//          Container(
//            width: kDouble_36,
//            height: kDouble_44,
//            alignment: Alignment.center,
//            decoration: BoxDecoration(border: Border(left: BorderSide(color: kGreyLineColor, width: 1.2))),
//            child: Column(
//              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//              children: [
//                InkWell(
//                  onTap: () {
//                    if (_controller.text == null || _controller.text.isEmpty) {
//                      _controller.text = '1';
//                    } else {
//                      _controller.text = (int.parse(_controller.text) + 1).toString();
//                    }
//                    onResult(int.parse(_controller.text));
//                  },
//                  child: Container(
//                    height: kDouble_22,
//                    width: kDouble_36,
//                    padding: const EdgeInsets.all(kDouble_6),
//                    child: UpArrowIcon(
//                      height: kDouble_16,
//                      width: kDouble_16,
//                    ),
//                  ),
//                ),
//                Divider(
//                  height: 0.0,
//                  thickness: 1.2,
//                  color: kGreyLineColor,
//                ),
//                InkWell(
//                  onTap: () {
//                    if (_controller.text == null || _controller.text.isEmpty) {
//                      _controller.text = '0';
//                    } else if (canNegative == false && _controller.text == '0') {
//                      print('return');
//                      return;
//                    } else {
//                      _controller.text = (int.parse(_controller.text) - 1).toString();
//                    }
//                    onResult(int.parse(_controller.text));
//                  },
//                  child: Container(
//                    height: kDouble_20,
//                    width: kDouble_36,
//                    padding: const EdgeInsets.all(kDouble_6),
//                    child: DownArrowIcon(
//                      height: kDouble_16,
//                      width: kDouble_16,
//                    ),
//                  ),
//                ),
//              ],
//            ),
//          ),
//        ],
//      ),
//    );
//  }
//}
//
//class DateRangePickerTextField extends StatelessWidget {
//  // final TextEditingController _startDateController = TextEditingController();
//  // final TextEditingController _endDateController = TextEditingController();
//  final ValueNotifier<String> _fromDateNotify = ValueNotifier<String>('');
//  final ValueNotifier<String> _toDateNotify = ValueNotifier<String>('');
//  final GlobalKey _buttonKey = GlobalKey();
//  final DateTime fromDate;
//  final DateTime toDate;
//  final int dayRange;
//  final Function(DateTime, DateTime) result;
//
//  DateRangePickerTextField({Key key, this.fromDate, this.toDate, this.result, this.dayRange}) : super(key: key) {
//    _fromDateNotify.value = DateFormat(DateTimeFormat.DD_MM_YYYY).format(fromDate);
//    _toDateNotify.value = DateFormat(DateTimeFormat.DD_MM_YYYY).format(toDate);
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      height: 44.0,
//      decoration: CustomDecoration.nonShadowDecoration(),
//      padding: EdgeInsets.symmetric(
//        horizontal: kDouble_16,
//      ),
//      child: InkWell(
//        key: _buttonKey,
//        splashColor: Colors.transparent,
//        onTap: () => _onTap(context),
//        child: Row(
//          mainAxisAlignment: MainAxisAlignment.spaceBetween,
//          children: [
//            ValueListenableBuilder<String>(
//              valueListenable: _fromDateNotify,
//              builder: (_, value, child) {
//                return Text(
//                  value,
//                  style: TextStyleCustom.regular14(),
//                );
//              },
//            ),
//            Text(
//              '-',
//              style: TextStyleCustom.regular14(),
//            ),
//            ValueListenableBuilder<String>(
//              valueListenable: _toDateNotify,
//              builder: (_, value, child) {
//                return Text(
//                  value,
//                  style: TextStyleCustom.regular14(),
//                );
//              },
//            ),
//          ],
//        ),
//      ),
//    );
//  }
//
//  void _onTap(BuildContext context) async {
//    AnimationGenericDialog(
//      widget: Material(
//        color: kWhiteColor,
////        child: DateTimePickerWidget(
////          selectedFirstDate: fromDate,
////          selectedLastDate: toDate,
////          onChange: _onChange,
////        ),
//      ),
//      parentKey: _buttonKey,
//    ).show(
//      locator<GlobalKey<NavigatorState>>().currentContext,
//      barrierDismissible: true,
//    );
//  }
//
//  void _onChange(DateTime firstDate, DateTime lastDate) {
//    DateTime lastDateReuslt = lastDate;
//    _fromDateNotify.value = DateFormat(DateTimeFormat.DD_MM_YYYY).format(firstDate);
//    if (lastDate != null) {
//      if (dayRange != null && lastDate.difference(firstDate).inDays > dayRange) {
//        lastDateReuslt = firstDate.add(Duration(days: dayRange));
//      } else {
//        lastDateReuslt = lastDate;
//      }
//      _toDateNotify.value = DateFormat(DateTimeFormat.DD_MM_YYYY).format(lastDateReuslt);
//    }
//    result(firstDate, lastDateReuslt);
//  }
//}
