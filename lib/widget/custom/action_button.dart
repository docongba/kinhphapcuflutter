import 'package:codebase/constant/ui_constant.dart';
import 'package:codebase/theme/colors.dart';
import 'package:codebase/theme/custom_decoration.dart';
import 'package:codebase/widget/custom/bk_icon.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class ActionButton extends StatelessWidget {
  final VoidCallback onTap;
  final String title;
  final String assetIcon;
  final Color buttonColor;
  final double width;
  bool selected;

  ActionButton({this.onTap, this.title = '', this.assetIcon = '', this.buttonColor = Colors.transparent, this.width = kDouble_60, this.selected = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: this.width,
      margin: EdgeInsets.only(right: kDouble_8, top: kDouble_6, bottom: kDouble_6),
      height: kDouble_32,
      child: FlatButton(
        padding: EdgeInsets.all(0.0),
        color: buttonColor,
        onPressed: () {
          onTap();
        },
        shape: CustomDecoration.shapeRoundedButton(radius: selected? kDouble_2: 0.0, borderColor: selected? kWhiteColor: Colors.transparent),
        child: BKIcon(
          resource: assetIcon,
          color: kWhiteColor,
        ),
      ),
    );
  }
}