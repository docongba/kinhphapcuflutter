class
AppString {
  static final String appBkName = 'BoodsKapper';
  static final String appBobName = 'Bob';

  // BUTTOn
  static final String btnInviteByOtherApps = 'Other Apps';
  static final String errorDynamicLabel = 'Dynamic Label';
  static final String btnNotNow = 'Not now';
  static final String btnRest = 'Reset';
  static final String btnCancel = 'Cancel';
  static final String btnNo = 'No';
  static final String btnClose = 'Close';
  static final String btnOk = 'Ok';
  static final String btnVerifyNow = 'Set Up Now';
  static final String btnYes = 'Yes';
  static final String btnUpdate = 'Update';
  static final String btnDone = 'Done';
  static final String btnAll = 'All';
  static final String btnConfirm = 'Confirm';
  static const String btnSndEmail = "Send Email";
  static const String btnSndInvite = "Send an invite";
  static final String btnFinishInspection = 'Finish Inspection';
  static final String btnSubmit = 'Submit';
  static final String btnStartNow = 'Start Now';
  static final String btnSave = 'Save';
  static final String btnAddNew = 'Add new';
  static final String btnNext = 'Next';
  static final String btnBack = 'Back';
  static final String btnProposeTime = 'Propose time';
  static final String btnClearAll = 'Clear all';
  static final String btnSkip = 'Skip';
  static final String btnContinue = 'Continue';
  static final String btnInviteNewUser = 'Invite new user';
//  static final String btnTagPicture = 'Tag Pictures';
  static final String btnTag = 'Tag';
  static final String btnUpload = 'Upload';
  static final String btnTakePhoto = 'Take Photo';
  static final String btnGallery = 'Gallery';
  static final String btnDelete = 'Delete';
  static final String btnStart = 'Start';
  static final String btnStop = 'Stop';
  static final String btnRegister = 'Sign up Here';
  static final String btnInviteClient = 'Invite Client';
  static final String btnInviteLandlord = 'Invite Landlord';
  static final String btnConfirmInspection = 'Confirm Inspection';
  static final String btnConfirmToolsAvailability = 'Confirm Tools Availability';
  static final String btnConfirmLocation = 'Confirm Location';
//  static final String btnRemoveRelationship = 'Remove Relationship';
//  static final String btnAddRelationship = 'Add Relationship';
  static final String btnAddOneMore = '+ Add new';
  static final String btnTryOtherEmail = 'Other emails';
  static final String btnOtherOptions = 'Other options';
  static final String btnInstallBob = 'Download Bob.ai';
  static final String btnDismiss = 'Dismiss';
  static final String btnWhyWeAreMirgrating = 'Why are we migrating BoodsKapper users to Bob.ai?';
  static final String titleSharingYourLocation = 'Please share your location. You can leave the app running in the background to continue tracking your location.';
  static final String btnShareLocation = 'Share Location';
  static final String btnLaterOn = 'Later';
  static final String btnAskStopSharingYourLocation = 'With this action, your location will be stopped sharing to everyone anymore. Are you sure?';
  static final String titleRequireToTurnOnLocationService = 'Location services are disabled. Would you like to enable them to start using Tracking Service?';
  static final String titleLocationNotificationTitle = 'Location Tracking';
  static final String titleLocationNotificationMessage = 'Location Tracking';
  static final String btnStartSharingYourLocation = 'Start sharing your location';
  static final String btnStopSharingYourLocation = 'Stop sharing your location';
  static final String btnMigrate = 'Migrate';
  static final String btnResetNotification = 'Reset Notification Settings';
  static final String btnSign = 'Sign';

  //TITLE
  static final String titlePleaseMaintainUsersInYourAccount = 'Please maintain users in your account.';
  static final String titleYouCanAddLeasingManagersForFinding = 'You can add Leasing Managers for finding leads on Bob.ai and Maintenance Technicians for inspections. ';
  static final String titleCustomerUserProvidedIsNotInTheBob = 'Сustomer user provided is not  in the Bob.ai network yet. ';
  static final String titleYouCanStillUseTheBobNetworkToFind = 'You can still use the Bob.ai network to find a housing unit  and stay connected. For the full experience reach out to your contacts at the Сustomer user provided and invite them to join Bob.ai';
  static final String titleYouCanContinueToUseTheBobToFindRentalLeads = 'You can continue to use the Bob.ai network to find rental leads. For more rental leads from your area reach out to the Customer user provided and invite them to join the Bob.ai network.';
  static final String titleDoYouWantToRemoveRelationship = 'Are you sure that you want to remove this relationship?';
  static final String titleDoYouWantToRemoveUser = 'Are you sure that you want to delete this user?';
  static final String titleYouAreVerifyingTheRelationshipCode = 'Your account is verifying. You can\'t do any actions without relationship';
  static final String titleAskSetUpRelationshipCode = 'Your account does not match to any our customers, please setup relationship.';
  static final String titleYouCanGenerateRftaAndEsignRTFA = 'You can generate and E-Sign RFTA and HAP by clicking here:';
  static final String titleIncomeChange = 'Income Change';
  static final String titleAnnualReCertification = 'Annual Re-certification';
  static final String titleChangeInFamilyComposition = 'Change in family composition';
  static final String titleHQSInspection = 'HQS Inspections';
  static final String titleRFTA = 'RFTA';
  static final String titleEmergencyRelocation = 'Emergency relocation';
  static final String titleHAPContract = 'HAP Contract';
  static final String titleRequestToPortOut = 'Request to Port Out';
  static final String titleGeneralQuestion = 'General questions';
  static final String titleChangeInspectionDate = 'Change Inspection Date';
  static final String titleChooseDate = 'Choose Date';
  static final String titleSearchIn = 'Search In';
  static final String titleLogin = 'Login';
  static final String titleAreYouSure = 'Are you sure?';
  static final String titleUpdateVersion = 'Update new version';
  static final String titleRequireGoogleDuo = 'Please open the Google Duo app and call #';
  static final String titleSocketConnectionError = 'Error. Please login again';
  static final String titleProfile = 'Profile';
  static final String titleAbout = 'About Us';
  static final String titlePrivacy = 'Privacy';
  static final String titleTermsUsed = 'Terms and Conditions';
  static final String titleSupport = 'Support';
  static final String titleMyAccounts = 'My Account';
  static final String titleUpdate = 'Update App';
  static final String titleSettings = 'Settings';
  static final String titleUpdateAccount = 'Update Profile';
  static final String titleResetPassword = 'Reset Password';
  static final String titleChatWithBKBot = 'Chat with BK(Bot)';
  static final String titleChatWithBobBot = 'Chat with Bob.ai';
  static final String titleMyInspections = 'My Inspections';
  static final String titleFilter = 'Filter';
  static final String titleFilterInspection = 'Filter Inspections';
  static final String titleInspections = 'Inspections';
  static final String titleChatCaseManager = 'Chat with my \nCase Manager';
  static final String titleCaseManager = 'Case Manager';
  static final String titleChatCaseManagerSingle = 'Chat with my Case Manager';
  static final String titleChannels = 'Channels';
  static final String titleChats = 'Chats';
  static final String titleBoodsKapperSupport = 'BoodsKapper Support';
  static final String titleStartMyDay = 'Start My Day';
  static final String titleVirtualInspection = 'Virtual Inspection';
  static final String titleNumberOfAttempts = 'Number of attempts:';
  static final String titlePickTheDateForReInspection = 'Pick the date for reinspection';
  static final String titleInconclusiveInspection = 'Inconclusive Inspection';
  static final String titleNoAccessInspection = 'No access inspection';
  static final String titlePleaseProvideNotes = 'Please provide notes';
  static final String titlePleaseProvideTheAddressOfOneUnite = 'Please provide the address of one unit you rented through the ';
  static final String titleTestMoveToInspectionDetail = 'Move to Inspection Detail';
  static final String titleUpdateUser = 'Update User';
  static final String titleConversations = 'Conversations';
  static final String titleSearchContact = 'Search Contact';
  static final String titleShowing = 'Showings';
  static final String titleHomeFinder = 'Home Finder';
  static final String titleFaqs = 'FAQs';
  static final String titlePleaseWait = 'Please Wait';
  static final String titleRemaining = 'Remaining';
  static final String titleCompleted = 'Completed';
  static final String titlePassed = 'PASS';
  static final String titleFailed = 'FAIL';
  static final String titleCanceled = 'CANCELED';
  static final String titleInConclusive = 'INCONCLUSIVE';
  static final String titleNoAccess = 'NO ACCESS';
  static final String titleFailDescription = 'Failure description';
  static final String titleDescription = 'Description';
  static final String titleOnboard1 = 'Address Book';
  static final String titleOnboard2 = 'Book results';
  static final String titleOnboard3 = 'Chat with BK(Bot)';
  static final String titleMore = 'More';
  static final String titleInspectionRaw = 'Inspection';
  static final String titleLocationShareBy = 'Location is shared by the';
  static final String titleToolIsAvailable = 'Tool availability is confirmed by the';
  static final String titleVideoStartWith = 'The video call will start with';
  static final String titleWhomDoYouWantToStartTheCallWith = 'Whom do you want to start the call with?';
  static final String titleAddNewUserOutsideTheBK = 'Add new user outside the BK';
  static final String titleAddNewUser = 'Add new Bob user';
  static final String titleAddNewPartnerUser = 'Add user';
  static final String titleUsers = 'User';
  static final String titleEmails = 'Email';
  static final String titlePhones = 'Phone number';
  static final String titleSelectUserRole = 'Select user role';
  static final String titlePicturePurpose = 'Picture purpose';
  static final String titleInformation = 'Information';
  static final String titleDeficiency = 'Deficiency';
  static final String titleSelectPhoto = 'Select Photo';
  static final String titleRoomLocation = 'Room location';
  static final String titleRoomNumber = 'Room number';
  static final String titleLeft = 'Left';
  static final String titleCenter = 'Center';
  static final String titleRight = 'Right';
  static final String titleFront = 'Front';
  static final String titleRear = 'Rear';
  static final String titleHitMicToStart = 'Hit the Mic and describe the deficiency and steps needed to remedy';
  static final String titleDoYouWantToProceed = 'Do you want to proceed?';
  static final String titleSetUpRelationship = 'Setup Relationship';
  static final String titleRelationshipType = 'Relationship Type';
  static final String titlePartnerType = 'Partner Type';
  static final String titleEnterYourRelationshipCode = 'Enter your relationship code';
  static final String titleCannotVerifyProvidedInformation = 'We are unable to verify with your provided information';
  static final String titleTheInformationYouProvidedDoesNotMatchOurRecords = 'The information you provided does not match our records. ';
  static final String labelPleaseReachOutToYourContactsAtCustomerUserEnterered = 'Please reach out to your contacts at Customer user entered and request them to invite you to Bob.ai ';
  static final String titleWeHaveNoInformationAboutYourRelationshipWithOurCustomers = 'We have no information about your relationship with our customers, please fill in this form.';
  static final String titleCases = 'Cases';
  static final String titleNotificationSettings = 'Notification Settings';
  static final String titleShowNotification = 'Show Notifications';
  static final String titleNotificationMessage = 'Message Notifications';
  static final String titleNotificationGroupChannels = 'Group and Channel messages';
  static final String titleNotificationFromBob = 'Notifications from Bob';
  static final String titleDDS = 'Service Delivery Report';
  static final String titleArrivalAt = 'Arrival time at';
  static final String titleDepartureTimeFrom = 'Departure time from';
  static final String titleClientSignature = 'Client Signature';
  static final String titleStaffSignature = 'Staff Signature';
  static final String titleDrawSignature = 'Draw Signature';
  static final String titleTypeSignature = 'Type Signature';
  static final String titleDrawYourSignatureBelow = 'Draw your signature below';
  static final String titleStaff = 'Staff';
  

  // LABEL
  static final String labelThankyou = 'Thank you!';
  static final String labelPleaseVerifyAccount = 'Please verify your account using';
  static final String labelSocialAccount = 'Social accounts';
  static final String labelSendACodeWeSent = 'Please enter the code we sent you via SMS message';
  static final String labelRetrySendSMSCode = 'Retry to send SMS code';
  static final String labelRetrySendEmailCode = 'Retry to send Email code';
  static final String laeblWrongSMScode = 'The code you entered is not correct. Please try again.';
  static final String labelOrPhoneNumber = 'or Phone numbers';
  static final String labelAddOneMore = 'Add one more number';
  static final String labelOtherOptionsToVerify = 'Other options to verify your account';
  static final String labelDoYouWantToSetUpMoreRalationship = 'Do you want to establish relationships with more agencies?';
  static final String labelYourRelationshipAs = 'Your relationship as a';
  static final String labelFirtNameOfTheUser = 'The first name of the user is';
  static final String labelContactToAddTheAccount = 'Please contact them to add you to the account. ';
  static final String labelYourRelationshipAsNowSetUp = 'is now set up.';
  static final String labelYourRelationshipAsNowApproval = 'is now in approval processing.';
  static final String labelWeCouldNotMatchYouToAnyCustomers = "We could match you to any of our customers. Please select your relationship to Customer selected below";
  static final String labelProvidedBy = 'provided by';
  static final String labelPartner = 'Partner';
  static final String labelPhoneNo = 'Phone';
  static final String labelOfficeNo = 'Office';
  static final String labelOfficePhoneNo = 'Office ';
  static final String labelStreet = 'Street ';
  static final String labelHouseNo = 'House # ';
  static final String labelHomePhoneNo = 'Home ';
  static final String labelHomeNo = 'Home';
  static final String labelAddressNo = 'Address';
  static final String labelConnectAccounts = 'Connect Accounts: ';
//  static final String labelBusinessPartnership = 'Business Relationship';
//  static final String labelUserInAccount = 'Users in my account ';
  static final String labelUpdatePartnerUser = 'Update user ';
  static final String labelEditPartnerUser = 'Edit user ';
  static final String labelPartnerName = 'Partner Name: ';
  static final String labelPhoneDot = 'Phone: ';
//  static final String labelHouseNo = 'House No: ';
  static final String labelCheckAuthority = ' is not in our network yet. At the moment there are no features that are useful to you. Soon we will be opening the HomeFinder for everyone and you will be able to find a housing unit with rents that are both affordable and reasonable. When this happens we will email you.';
  static final String labelHintRelationshipCode = 'This is a code you may have received from Housing Authority of the City of Montgomery in a letter. If you have not received it don’t worry about it. We will verify you through other means.';
  static final String labelIfThereIsNoUnitProvide = 'If there is no unit provided we can’t establish a relationship with a PHA. \n\nHowever you can continue to use Bob.ai and get leads (Rental opportunities) by clicking the Rental opportunities link on the left. Filter by the city where you have units and select “Schedule a showing”. \n\nIf the client and you agree to move forward after a showing you can create the RFTA electronically and sign it online. When your RFTA is approved the relationship with the PHA will be automatically established.';
  static final String labelPleaseSelectPHAsYouHave = 'Please select the PHAs you have relationships with. You can add multiple PHAs';
  static final String labelDoYouHaveAUnitRented = 'Do you have a unit rented to a client in the HCV program?';
  static final String labelIAmAnApplicantForHCV = 'I am an applicant for HCV';
  static final String labelApplicantMessageYouCanParticipateInTheDiscussion = 'As an applicant, you can participate in the discussions on the Applicant channel and sign up for events. Case Managers from various PHAs are available on the channel. \n\nYou SHOULD NOT rely on Bob.ai for application deadlines and instructions.';
  static final String labelIAmAnApplicantForPublicHousing = 'I am an applicant for Public Housing';
  static const String labelRoleResidentPublicHousing = 'Resident - Public Housing';
  static final String labelRoleClientHCV = 'Client - HCV (Section 8) program';
  static final String labelHouseAuthority = 'Housing Authority';
  static final String labelSearching = 'Searching...';
  static final String labelDoYouHaveARelationshipCode = 'Do you have a relationship code?';
  static final String labelRelationshipCode = 'Relationship code';
  static final String labelUnitStreetAddress = 'Unit street address';
  static final String labelZipCode = 'Zip code';
  static const String labelChangeInspectionDate = 'Please change the date and time only in exceptional situations as the date and time have been communicated to the client and landlord.';
  static const String labelYouWillSeeAllTheleads = 'You will see all the leads available. Click on Schedule Showing and Bob will guide you through the affordability and reasonableness checks and help you create your RFTA. If you are working'
      'with a client ask them for the lead number.\n\nDo you still need to chat with a Case manager?';
  static const String labelNotifyUserHasInformationTagOnly = 'You have tagged only informational picture(s). Tag atleast one deficiency before submitting.';

  static final String labelPleaseNoteThatTheStartTimeIsNotExact = 'Please note that the Start time is not exact';
  static final String labelWeWillStartRunningACountdownHereWhen = 'We will start running a countdown here when we are sure of the start time';
  static final String labelConfirmLocationIsTheFeatureToEnsure = 'Confirm Location is the feature to ensure that you are at the right unit. You can only'
      'do this 10 minutes before start of the inspection';
  static final String labelAsOfNowWeOnlyHaveTheApproximateStartTimeOf = 'As of now we only have the approximate Start time of';
  static final String labelTheAddressIsSharedIs = 'The address is shared is';
  static final String labelThisIsNotTheUnitAddress = 'This is not the Unit address';
  static final String label = 'If this is a mistake, don’t worry. Inspector can override this. '
      'Inspection will start at any time now. \nWhen the inspector opens the Zoom room the icon below will turn green and you will be able to join the room. '
      '\nInspector will walk you through the Inspection steps. The first step '
      'is usually to verify the door number( Appt#) for a multi family unit.';
  static final String labelDoYouWantToTagMore = 'Do you want to tag more pictures?';
  static final String labelCheckEmail = 'Please check your email.';
  static final String labelSelectDefects = 'Select the defects for';
  static final String labelSelectInformations = 'Select options for ';
  static final String labelOrEmailAddress = 'Email Addresses ';
  static final String labelSelectRoom = 'Select Room';
  static final String labelOnboard1 = 'Start chatting with clients, landlords and PHA colleagues.';
  static final String labelOnboard2 = 'Book results on inspections every day to keep rescheduling streamlined.';
  static final String labelOnboard3 = 'Chat with BK(Bot) if you have questions';
  static final String labelLoading = 'Loading';
  static final String labelFromDate = 'From date';
  static final String labelToDate = 'To date';
  static final String labelRegister = 'Sign up';
  static final String labelOpenInGoogleMaps = 'Open in Google Maps';
  static final String labelEmail = 'Email';
  static final String labelPassword = 'Password';
  static final String labelConfirmPassword = 'Confirm password';
  static final String labelOldPassword = 'Old password';
  static final String labelNewPassword = 'New password';
  static final String labelReInputNewPassword = 'Fill new password';
  static final String labelLogin = 'Sign in';
  static final String labelGoogleLogin = 'Sign in with Google';
  static final String labelAppleLogin = 'Sign in with Apple';
  static final String labelFacebookLogin = 'Sign in with Facebook';
  static final String labelLinkedInLogin = 'Sign in with Linked In';
  static final String labelGoogleSignUp = 'Sign up with Google';
  static final String labelAppleSignUp = 'Sign up with Apple';
  static final String labelFacebookSignUp = 'Sign up with Facebook';
  static final String labelLinkedInSignUp = 'Sign up with Linked In';
  static final String labelForgotPassword = 'Forgot password';
  static final String labelSkip = 'Skip';
  static final String labelSignUp = 'Sign Up';
  static final String labelFullName = 'Full name';
  static final String labelPhoneNumber = 'Phone number';
  static final String labelGender = 'Gender';
  static final String labelDateOfBirth = 'Dob';
  static final String labelDate = 'Date';
  static final String labelChangePassword = 'Change password';
  static final String labelSetPassword = 'Set new password';
  static final String labelMale = 'Male';
  static final String labelFemale = 'Female';
  static final String labelOther = 'Other';
  static final String labelNotification = 'Notification';
  static final String labelSearch = 'Search';
  static const String labelHelp = 'Help';
  static const String labelLogout = 'Log Out';
  static const String labelWelcomeBack = 'Welcome back';
  static const String labelDontRememberYourPassword = "Don't remember your password?";
  static const String pleaseEnterYourEmailToResetPassword = "Please enter your email. We will send you an email to confirm the password change.";
  static final String labelPhysical = 'Physical';
  static final String labelVirtual = 'Virtual';
  static final String labelStartTime = 'Start Time:';
  static final String labelStartTimeRaw = 'Start Time';
  static final String labelEndTime = 'End Time:';
  static final String labelEndTimeRaw = 'End Time';
  static final String labelMedium = 'Medium';
  static final String labelAddress = 'Address';
  static final String labelClient = 'Client:';
  static final String labelLandlord = 'Landlord:';
  static const String labelClientRaw = 'Client';
  static const String labelLandlordRaw = 'Landlord';
  static const String labelApplicant = 'Applicant';
//  static final String labelStartVideo = 'Start Video';
//  static final String labelGroupChat = 'Group';
  static final String labelGroups = 'Groups';
//  static final String labelBookResult = 'Book Result';
  static final String labelDocumentResult = 'Document Result';
  static final String labelStopTrackingLocation = 'Stop Tracking Location';
  static final String labelResultOfInspection = 'Result of Inspection';
  static final String labelSelectFrom = 'Select From:';
  static final String labelWhoIsResponsibleForFixing = 'Who is responsible for fixing the deficiency?';
  static final String labelWhoIsResponsibleForInformation = 'Who is responsible for this information?';
  static final String labelBoth = 'Both';
  static final String labelAbatement = 'Abatement';
  static final String labelTermination = 'Termination';
  static final String labelMissingLandlordInformation = 'Missing Landlord Contact Information';
  static final String labelMissingClientInformation = 'Missing Client Contact Information';
  static final String labelStatus = 'Status';
  static final String labelStatusSuccess = 'Success';
  static final String labelStatusCall = 'Call';
  static final String labelStatusFail = 'Fail';
  static final String labelStatusLocationVerified = 'Location Verified';
  static final String labelVerifiedDate = 'Verified date';
  static final String labelVerifiedBy = 'Verified by';
  static final String labelHaveNoPermissionToAddNewPartnerUser = 'Your account have no permission to add new partner user. Please set up your relationship first';
  static final String labelEmailProvidedBy = 'E-mail provided by';
  static final String labelRoles = 'Roles';
  static final String labelRole = 'Role';
  static final String labelUserName = 'User Name';
  static final String labelPhone = 'Phone';
  static final String labelHintPhone = 'Phone number';
  static final String labelHintOfficePhone = 'Office number';
  static final String labelHintHomePhone = 'Home number';
  static final String labelInspectionProgress = 'Inspection progress:';
  static final String labelNA = 'N/A';
  static final String labelEmptyMyInspections = 'There are no inspections';
  static final String labelEmptyMyRemainInspections = '';
  static final String labelEmptyMyCompletedInspections = '';
//  static final String labelEmptyMyRemainInspections = '';
  static final String labelEmptyInspections = 'There are no Inspections. Please select other options to query again by clicks on Filter icon';
  static final String labelEmptyInspectionFilter = 'There are no inspections for your search criteria. You can search again by selecting the filter icon on the top. ';
  static final String labelEmptyInspectionsSearchingPre = 'There are no inspections on';//There are no inspections on, You can change your search criteria by clicking on the filter icon above.
  static final String labelEmptyInspectionsSearchingPost = '. You can change the search criteria by clicking on the filter icon above.';//There are no inspections on, You can change your search criteria by clicking on the filter icon above.
  static final String labelSelectAnOption = 'Select an option:';
  static final String labelCancelOption = 'Why do you want to cancel this inspection?';
  static final String labelSelectAll = 'Select All';
  static final String labelHeadOfHouseHold = 'Name of the head of household';
  static final String labelBusinessName = 'Business name';
  static final String labelNoPhone = 'phone number is missing';
  static final String labelName = 'name';
  static final String labelReInspection = 'Reinspection';
  static final String labelAbatementAndTermination = 'Abatement and Termination';
  static final String labelChooseOptionBelow = 'Please choose from the options below';
  static final String labelChooseOption = 'Please choose an option';
  static final String labelAnyTimDuringThwWorkday = 'Any time during the workday is good';
  static final String label2HourWindow = '2-hour window';
  static final String labelFollowUpActions = 'What is the follow-up action?';
  static final String labelWaiting = 'Waiting...';
  static final String labelEmptySlot = 'There are not slots';
  static final String labelEmptyRoom = 'There are no rooms';
  static final String labelDone = 'Done!';
  static final String labelTenant = 'Tenant';
  static final String labelWriteYourMessageHere = 'Write your message here';
  static final String labelWriteYourDescriptionHere = 'Write your description here';
  static final String labelMandatoryDescription = 'You must add your description before moving to next step';
  static final String labelOptimizerIsLookingForTheInspector = 'Optimizer is looking for the inspector and time slots based on planned schedules. This may take about 2 minutes.';
  static final String labelThisInspectionWillBeMarked = 'This inspection will be marked as';
  static final String labelReason = 'Reason: ';
  static final String labelTheInspectionWillScheduledOn = 'The inspection will be scheduled on:';
  static final String labelAllResult = 'All results';
  static final String labelDateInValid = 'You can not select a date before the current date';
  static final String hintSendMessage = 'Message';
  static final String labelSelectDate = 'Date';
  static final String labelSelectTime = 'Time';
  static final String labelFirstName = 'First name';
  static final String labelCity = 'City';
  static final String labelState = 'State';
  static final String labelLastName = 'Last name';
  static final String labelIsAndroidUser = 'is an Android user';
  static final String labelThisUserHaveNotInstalledApp = 'have not installed apps yet';
  static final String labelIsiOSUser = 'is an iOS user';
  static final String labelHasProvidedTheNumber = 'has provided the number';
  static final String labelAndroidGoogleDuoWithPhoneNumber = 'Please initiate a Google Duo call with them.';
  static final String labelAndroidGoogleDuoWithoutPhoneNumber = 'Please get their mobile number and initiate the Google Duo call with them.';
  static final String labeliOSFacetimeWithPhoneNumber = 'Do you want to start the FaceTime call with this number?';
  static final String labeliOSFacetimeWithAppleId = 'Do you want to start the FaceTime call with';
  static final String labeliOSFacetimeWithoutPhoneAppleId = 'Please get their mobile number and initiate the Facetime call with them';
  static final String labelNow = 'Now?';
  static final String labelIsTheirPhone = 'is their phone number';
  static final String labelEmptyUsers = 'There are no Bob users';
  static final String labelInspectionIsCompleted = 'The inspection is completed already. Do you want to start the inspection again?';
  static final String labelPictureRelatedToADeficiency = 'Pictures related to a deficiency should be tagged. Tagging of informational pictures is optional.';
  static final String labelNoPictureToTag = 'No Pictures to Tag';
  static final String labelIamListening = 'I\'m listening...';
  static final String labelIamNotListening = 'Not listening';
  static final String labelTaggingPictureAtLeast1Picture = 'To fail an inspection at least one picture should be tagged as deficiency.';
  static final String labelConfirmCaseManagerPrefix = 'When you join the chat please provide information about ';
  static final String labelConfirmCaseManagerPostfix = ' without waiting for the Case Manager to enter the chat. You will be notified when the Case Manager responds. Do you want to proceed?';
  static final String labelPleaseWaitForInspectionToStart = 'Please wait for inspection to start, an inspector will call you or you can join from here';
  static final String labelPleaseSelectYourRelationshipTypeBelow = 'Please select your relationship type below.';
  static final String labelPlaceholderSearching = 'Example: Dallas';
  static final String labelPlaceholderCode = 'Example: 1234';
  static final String labelAreYouMissing = 'Are you missing data? You may have logged in previously with another account.';
  static final String labelPleaseLinkAccounts = 'Please link your accounts';
  static final String labelOwnershipVerified = 'Ownership verified by';
  static final String labelRelationshipoCodeVerified  = 'Relationship code verified by';
  static final String labelFailRelationshipCode = 'The relationship code you entered does not match our records.';
  static final String labelTrySetRelationshipCode = 'Let us try to set up the relationship another way.';
  static final String labelCodeReadyInUse = 'This relationship code is already in use. ';
  static final String labelEnterCodeWeSentToPhone = 'Please enter the code we sent you via SMS message through ';
  static final String labelEnterCodeWeSentToEmail = 'Please enter the code we sent you via email';
  static final String labelRetrySMSCode = 'Retry to send to receive the code -';
  static final String labelPhoneHint = 'Ex: +11234567890';

  static final String labelSuperUserAtPHAsReceiveMany = 'Superusers at PHAs receive many requests from clients and landlords asking for assistance with signing up and setting up user accounts. Since they use the BoodsKapper app, they find it difficult to help others sign up on Bob.ai. '
      '\n\nTherefore, we redesigned the app to make everyone\'s UI the same. As an example, clients and landlords see the same Inspections dashboard but only with the inspections that apply to them.'
      '\n\nWe are noticing that clients and landlords are trying to create accounts in BoodsKapper; however, they are not able to complete the set up and this results in significant frustration. ';

  static final String labelTheMigrationProcessIsVerySimple = 'The migration process is very simple. Please login with the same e-mail address with the extra step of reentering the password';
  static final String labelMirgrationProcessIsSimple = 'Migration process is simple. On the same device, your account will be transferred to ';
  static final String labelAutomatically = ' automatically';
  static final String labelBob = 'Bob.ai';
  static final String labelNoNewFeaturesWillBeAddedToThisPage = '* No new features will be added to the BoodsKapper mobile app and the BoodsKapper app will be retired on \nDec 31, 2020.';
  static final String labelNotAssigned = 'Not Assigned';
  static final String labelEmptyCases = 'There are no cases.';
  static final String labelNotMatchPassword  = 'Password and confirm password must match';
  static final String labelConfirmPassLength = 'Confirm Password length must be at least 8 characters';
  static final String labelConfirmPassRequired = 'Confirm Password is required';
  static final String labelPassLength = 'Password length must be at least 8 characters';
  static final String labelPassRequired = 'Password is required';
  static final String labelNotificationReset = 'Reset all notification settings, including custom notification settings for your chats.';

  // ERROR
  static final String errEmptyReason = 'Fill your reason';
  static final String errEmptyEmail = 'Email must not be empty';
  static final String errInvalidEmail = 'Email is not valid';
  static final String errInvalidZipcode = 'ZipCode is not valid';
  static final String errInvalidPhoneNumber = 'Phone number is not valid';
  static final String errEmptyPassword = 'Password must not be empty';
  static final String errEmptyConfirmPassword = 'Confirm password is not empty';
  static final String errConfirmPasswordNotMatch = 'Confirm password is not match';
  static final String errEmptyNewPassword = 'New password is not empty';
  static final String emptySlot = 'No available slot found. \nPlease select another day';
  static final String emptyTag = 'There are no tagged pictures';
  static final String emptyRoute = 'There are no routes for selected date';
  static final String emptyAddedTag = 'There are no tag(s) added to this picture. Please add atleast one.';
  static final String errorSMSCode = 'The code you entered is not correct. Please try again.';
  // HINT

  // MESSAGE
  static final String messageAccomplishedWorkingDay = 'You have accomplished your work today.';
  static final String messageCongratulateFinishWorkingDayWithPhysical = 'Thanks. All inspections results for today are booked. Location service is turned off now';
  static final String messageCongratulateFinishWorkingDayWithVirtual = 'Thanks. All inspections for today are booked';
  static final String messageInspectionIsNotDueYet = 'Inspection is not due yet. Do you want to proceed?';
  static final String messageCongratulationSuccess = 'Congratulations!';
  static final String messageCongratulationSuccessMessage = 'The result is posted on the group chat';
  static final String messageYourMessageIsPosted = 'Your message is posted to the group chat.';
  static final String messagePostedYourNote = 'Ok, I posted your notes on the group chat';
  static final String messageUpdateAppVersion = 'Please update the latest version from the store';
  static final String messageDoYouWantToStartTheInspectionNowPost = 'Do you want to start the inspection now?';
  static final String messageDoYouWantToStartTheInspectionNowPre = 'The inspection date is ';
  static final String messageEmptyFirstNameLastName = 'First name and Last name should not be empty';
  static final String messageEmptyMandatoryField = 'Mandatory field(s) should not be empty';
  static final String messagePhoneEmailErrorField = 'Email is not in correct format';
  static final String messageSubmitFormSuccessful = 'You have successfully submitted the tagged picture(s).';

  static final String messageLocationIsNotConfirmed = 'The inspection location is not confirmed.';
  static final String messageTheInspectionAssignedPre = 'The inspection is assigned to ';
  static final String messageThereInInspectionWithoutAssigned = 'The inspection is not assigned to anyone';
  static final String messageTheInspectionAssignedPost = '. Do you want to start the inspection now?';
  static final String messageWhatTimeDoYouExpectToStart = 'What time do you expect to start the';
  static final String messageDeletePhoto = 'Are you sure you want to delete this tagged picture?';
  static final String messageSavedFile = 'Saved file successful';
  static final String messageRecommendAddDescriptionTag = 'Do you want to add a description for the picture?';
  static final String messageInviteByEmail = 'Invite by Email';
  static final String messageInviteByPhoneNumber = 'Invite by Phone number';
  static final String titleInviteUser = 'Invite user to Bob';
  static final String titleLinkedMessagePre = 'Congratulation! You linked your ';
  static final String titleLinkedMessagePost = 'account successful';
  static final String titleUnLinkMessagePre = 'Are you sure you want to unlink your';
  static final String titleUnLinkMessagePos = 'account?';
  static final String titleOpenTickets = 'The list of your open tickets';
  static final String titleDeviceFromBK = 'Please select your ID to migrate from BoodsKapper to Bob.ai';
  static final String titlePasswordReset = 'Password Reset';
  static final String titlePasswordResetSuccessful = 'Your Password has been reset successfully. From now you can use this account with reset password to login Bob.ai.';
  static final String titleYouHaveSubmittedInformationAboutRelationship = 'You have submitted information about relationship with';

//      '\nGoogle: https://play.google.com/store/apps/details?id=com.bob.ai&hl=en&gl=US';
//  static final String messageInviteUserToBk = 'Hi < first name> inspection for < street address> is scheduled for < date> <time>. '
//      '\nPlease download the Bob.ai mobile app. We will post the results and pictures on this app. '
//      '\nApple: https://apps.apple.com/us/app/bob-ai/id1449970152'
//      '\nGoogle: https://play.google.com/store/apps/details?id=com.bob.ai&hl=en&gl=US';

  static String messageInviteToBob(String firstName, String address, String inspectionDate, String inspectionTime){
    return 'Hi $firstName, \nInspection for $address is scheduled for $inspectionDate $inspectionTime.'
        '\nPlease download the Bob.ai mobile app. We will post the results and pictures on this app. You can download using these two links.'
        '\nApple: https://apps.apple.com/us/app/bob-ai/id1449970152'
        '\nGoogle: https://play.google.com/store/apps/details?id=com.bob.ai&hl=en&gl=US';
  }

  static String messageInviteToBk(String firstName, String address, String inspectionDate, String inspectionTime){
    return 'Hi $firstName, \nInspection for $address is scheduled for $inspectionDate $inspectionTime.'
        '\nPlease download the Boodskapper mobile app. We will post the results and pictures on this app. You can download using these two links.'
        '\nApple: https://apps.apple.com/us/app/boodskapper/id1399430707'
        '\nGoogle: https://play.google.com/store/apps/details?id=com.boodskapper.android';
  }


  static final String linkTerms = 'https://www.boodskapper.com/acceptableusepolicy';
  static final String linkAbout = 'https://www.boodskapper.com/about-us';
  static final String linkPrivacy = 'https://www.boodskapper.com/privacy-policy';
  static final String linkBob = 'https://bob.ai/';

  static final String linkBobPrivacy = 'https://www.boodskapper.com/privacy-policy';
  static final String linkBobTerms = 'https://www.boodskapper.com/acceptableusepolicy';


  static final List<Map> states = [
    {"name":"SELECT A STATE","id":"EMPTY","display":"SELECT A STATE","value":"SELECT A STATE"},
    {"name":"ALABAMA","id":"AL","display":"ALABAMA","value":"ALABAMA"},
    {"name":"ALASKA","id":"AK","display":"ALASKA","value":"ALASKA"},
    {"name":"AMERICAN SAMOA","id":"AS","display":"AMERICAN SAMOA","value":"AMERICAN SAMOA"},
    {"name":"ARIZONA","id":"AZ","display":"ARIZONA","value":"ARIZONA"},
    {"name":"ARKANSAS","id":"AR","display":"ARKANSAS","value":"ARKANSAS"},
    {"name":"CALIFORNIA","id":"CA","display":"CALIFORNIA","value":"CALIFORNIA"},
    {"name":"COLORADO","id":"CO","display":"COLORADO","value":"COLORADO"},
    {"name":"CONNECTICUT","id":"CT","display":"CONNECTICUT","value":"CONNECTICUT"},
    {"name":"DELAWARE","id":"DE","display":"DELAWARE","value":"DELAWARE"},
    {"name":"DISTRICT OF COLUMBIA","id":"DC","display":"DISTRICT OF COLUMBIA","value":"DISTRICT OF COLUMBIA"},
    {"name":"FLORIDA","id":"FL","display":"FLORIDA","value":"FLORIDA"},
    {"name":"GEORGIA","id":"GA","display":"GEORGIA","value":"GEORGIA"},
    {"name":"GUAM","id":"GU","display":"GUAM","value":"GUAM"},
    {"name":"HAWAII","id":"HI","display":"HAWAII","value":"HAWAII"},
    {"name":"IDAHO","id":"ID","display":"IDAHO","value":"IDAHO"},
    {"name":"ILLINOIS","id":"IL","display":"ILLINOIS","value":"ILLINOIS"},
    {"name":"INDIANA","id":"IN","display":"INDIANA","value":"INDIANA"},
    {"name":"IOWA","id":"IA","display":"IOWA","value":"IOWA"},
    {"name":"KANSAS","id":"KS","display":"KANSAS","value":"KANSAS"},
    {"name":"KENTUCKY","id":"KY","display":"KENTUCKY","value":"KENTUCKY"},
    {"name":"LOUISIANA","id":"LA","display":"LOUISIANA","value":"LOUISIANA"},
    {"name":"MAINE","id":"ME","display":"MAINE","value":"MAINE"},
    {"name":"MARSHALL ISLANDS","id":"MH","display":"MARSHALL ISLANDS","value":"MARSHALL ISLANDS"},
    {"name":"MARYLAND","id":"MD","display":"MARYLAND","value":"MARYLAND"},
    {"name":"MASSACHUSETTS","id":"MA","display":"MASSACHUSETTS","value":"MASSACHUSETTS"},
    {"name":"MICHIGAN","id":"MI","display":"MICHIGAN","value":"MICHIGAN"},
    {"name":"MINNESOTA","id":"MN","display":"MINNESOTA","value":"MINNESOTA"},
    {"name":"MISSISSIPPI","id":"MS","display":"MISSISSIPPI","value":"MISSISSIPPI"},
    {"name":"MISSOURI","id":"MO","display":"MISSOURI","value":"MISSOURI"},
    {"name":"MONTANA","id":"MT","display":"MONTANA","value":"MONTANA"},
    {"name":"NEBRASKA","id":"NE","display":"NEBRASKA","value":"NEBRASKA"},
    {"name":"NEVADA","id":"NV","display":"NEVADA","value":"NEVADA"},
    {"name":"NEW HAMPSHIRE","id":"NH","display":"NEW HAMPSHIRE","value":"NEW HAMPSHIRE"},
    {"name":"NEW JERSEY","id":"NJ","display":"NEW JERSEY","value":"NEW JERSEY"},
    {"name":"NEW MEXICO","id":"NM","display":"NEW MEXICO","value":"NEW MEXICO"},
    {"name":"NEW YORK","id":"NY","display":"NEW YORK","value":"NEW YORK"},
    {"name":"NORTH CAROLINA","id":"NC","display":"NORTH CAROLINA","value":"NORTH CAROLINA"},
    {"name":"NORTH DAKOTA","id":"ND","display":"NORTH DAKOTA","value":"NORTH DAKOTA"},
    {"name":"OHIO","id":"OH","display":"OHIO","value":"OHIO"},
    {"name":"OKLAHOMA","id":"OK","display":"OKLAHOMA","value":"OKLAHOMA"},
    {"name":"OREGON","id":"OR","display":"OREGON","value":"OREGON"},
    {"name":"PALAU","id":"PW","display":"PALAU","value":"PALAU"},
    {"name":"PENNSYLVANIA","id":"PA","display":"PENNSYLVANIA","value":"PENNSYLVANIA"},
    {"name":"PUERTO RICO","id":"PR","display":"PUERTO RICO","value":"PUERTO RICO"},
    {"name":"RHODE ISLAND","id":"RI","display":"RHODE ISLAND","value":"RHODE ISLAND"},
    {"name":"SOUTH CAROLINA","id":"SC","display":"SOUTH CAROLINA","value":"SOUTH CAROLINA"},
    {"name":"SOUTH DAKOTA","id":"SD","display":"SOUTH DAKOTA","value":"SOUTH DAKOTA"},
    {"name":"TENNESSEE","id":"TN","display":"TENNESSEE","value":"TENNESSEE"},
    {"name":"TEXAS","id":"TX","display":"TEXAS","value":"TEXAS"},
    {"name":"UTAH","id":"UT","display":"UTAH","value":"UTAH"},
    {"name":"VERMONT","id":"VT","display":"VERMONT","value":"VERMONT"},
    {"name":"VIRGIN ISLANDS","id":"VI","display":"VIRGIN ISLANDS","value":"VIRGIN ISLANDS"},
    {"name":"VIRGINIA","id":"VA","display":"VIRGINIA","value":"VIRGINIA"},
    {"name":"WASHINGTON","id":"WA","display":"WASHINGTON","value":"WASHINGTON"},
    {"name":"WEST VIRGINIA","id":"WV","display":"WEST VIRGINIA","value":"WEST VIRGINIA"},
    {"name":"WISCONSIN","id":"WI","display":"WISCONSIN","value":"WISCONSIN"},
    {"name":"WYOMING","id":"WY","display":"WYOMING","value":"WYOMING"}
  ];

  static final List<String> favorites = [
    'Income change',
    'Change in family comosition',
    'HQS Inspections',
    'RFTA',
    'Emergency relocation',
    'HAP Contract',
    'General questions',
  ];
  static final List<String> favoritesResident = [
    'Income change',
    'Change in family comosition',
    'Maintenance Request',
    'Rent Amount',
    'Dispute with other residents',
    'Utility Allowance',
    'Disagreement with Property Manager',
    'Reasonable Accommodation',
    'General questions',
  ];
  static final List<String> favoritesLL = [
    'RFTA',
    'HQS Inspections',
    'HAP Contract',
    'Rent Increase',
    'General questions',
  ];
  static final List<Map> roles = [
    {
      "display": "Resident - Public Housing",
      "value": "Resident",
    },
    {
      "display": "Client- HCV (Section 8) program",
      "value": "Client",
    },
    {
      "display": "Landlord",
      "value": "Landlord",
    },
    {
      "display": "Applicant",
      "value": "Applicant",
    }
  ];
}

