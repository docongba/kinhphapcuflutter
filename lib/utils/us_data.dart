import 'dart:convert';

import 'package:flutter/cupertino.dart';

class UsData{

  static UsData _instance = new UsData.internal();

  UsData.internal();

  factory UsData() => _instance;

  BuildContext _context;

  initUsData(BuildContext  context){
    this._context = context;
  }

  List<StateObj> getUsStates(){
    List<StateObj> stateObjects = [];
    Map<String, String> states =
    {
      'Alabama': 'AL',
      'Alaska': 'AK',
      'American Samoa': 'AS',
      'Arizona': 'AZ',
      'Arkansas': 'AR',
      'California': 'CA',
      'Colorado': 'CO',
      'Connecticut': 'CT',
      'Delaware': 'DE',
      'District Of Columbia': 'DC',
      'Federated States Of Micronesia': 'FM',
      'Florida': 'FL',
      'Georgia': 'GA',
      'Guam': 'GU',
      'Hawaii': 'HI',
      'Idaho': 'ID',
      'Illinois': 'IL',
      'Indiana': 'IN',
      'Iowa': 'IA',
      'Kansas': 'KS',
      'Kentucky': 'KY',
      'Louisiana': 'LA',
      'Maine': 'ME',
      'Marshall Islands': 'MH',
      'Maryland': 'MD',
      'Massachusetts': 'MA',
      'Michigan': 'MI',
      'Minnesota': 'MN',
      'Mississippi': 'MS',
      'Missouri': 'MO',
      'Montana': 'MT',
      'Nebraska': 'NE',
      'Nevada': 'NV',
      'New Hampshire': 'NH',
      'New Jersey': 'NJ',
      'New Mexico': 'NM',
      'New York': 'NY',
      'North Carolina': 'NC',
      'North Dakota': 'ND',
      'Northern Mariana Islands': 'MP',
      'Ohio': 'OH',
      'Oklahoma': 'OK',
      'Oregon': 'OR',
      'Palau': 'PW',
      'Pennsylvania': 'PA',
      'Puerto Rico': 'PR',
      'Rhode Island': 'RI',
      'South Carolina': 'SC',
      'South Dakota': 'SD',
      'Tennessee': 'TN',
      'Texas': 'TX',
      'Utah': 'UT',
      'Vermont': 'VT',
      'Virgin Islands': 'VI',
      'Virginia': 'VA',
      'Washington': 'WA',
      'West Virginia': 'WV',
      'Wisconsin': 'WI',
      'Wyoming': 'WY'
    };

    states.forEach((key, value) {
      stateObjects.add(StateObj(shortName: value, name: key));
    });

    print("getStates ${stateObjects[0].name}");
    return stateObjects;
  }

  Future<List<String>> getCities({String state = 'Texas'}) async{
    String data = await DefaultAssetBundle.of(_context).loadString("assets/US_States_and_Cities.json");
    final jsonResult = json.decode(data);

    List<String> cities = [];


    var elements = jsonResult[state] as List;
    cities = elements.map((object) => object as String).toList();
    cities.sort((a, b) => a.compareTo(b));

  return cities;
  }
}

class StateObj{
  String name;
  String shortName;

  StateObj({this.shortName, this.name});
}

