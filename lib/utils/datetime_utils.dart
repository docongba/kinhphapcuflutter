import 'package:codebase/constant/properties_constant.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DateTimeUtils {
  static String toDDMMYYYY(DateTime date) {
    // return date.day.toString() + '/' + date.month.toString() + '/' + date.year.toString();
    return DateFormat(DateTimeFormat.DD_MM_YYYY).format(date);
  }

  static String covert24FormatToAPm(String inputHour){
    try{
      var value = DateFormat.Hm().format(DateFormat("hh:mm").parse(inputHour));

      TimeOfDay timeType = TimeOfDay(hour: int.parse(inputHour.split(':')[0]), minute: int.parse(inputHour.split(':')[1])); // 3:00 PM
      return '$value ${timeType.period == DayPeriod.am? 'AM':'PM'}';//value ;
    }catch (e){
      return inputHour;
    }
  }

  static String toYYYYMMDD(DateTime date) {
    // return date.day.toString() + '/' + date.month.toString() + '/' + date.year.toString();
    return DateFormat(DateTimeFormat.MMDDYYYY).format(date);
  }

  static String dateToString(DateTime date, String format){
    var formatter = DateFormat(format);
    var today = formatter.format(date);
    return today;
  }
  
  /// Nếu `inputDate` là thứ 7 hoặc chủ nhật, trả về thứ 6, 
  /// Nếu trong phạm vi 2-3-4-5-6, trả về `inputDate`
 DateTime calculateWorkingDay(DateTime inputDate) {
   print("INPUT DATE   $inputDate    ${inputDate.weekday}");
    DateTime result;
    if (inputDate.weekday == 7) {
      result = inputDate.add(Duration(days: -2));
    } else if (inputDate.weekday == 6) {
      result = inputDate.add(Duration(days: -1));
    } else {
      print("alksjdlkasjkdlsa");
      result = inputDate;
    }
    return result;
  }

  static String timeAgoSinceDate(String dateString, {bool numericDates = true}) {
    DateTime date = DateTime.parse(dateString);

    final date2 = DateTime.now();
    final difference = date2.difference(date);


//    if ((difference.inDays / 365).floor() >= 2) {
//      return '${(difference.inDays / 365).floor()} years ago';
//    } else if ((difference.inDays / 365).floor() >= 1) {
//      return (numericDates) ? '1 year ago' : 'Last year';
//    } else if ((difference.inDays / 30).floor() >= 2) {
//      return '${(difference.inDays / 365).floor()} months ago';
//    } else if ((difference.inDays / 30).floor() >= 1) {
//      return (numericDates) ? '1 month ago' : 'Last month';
//    } else
    if ((difference.inDays / 7).floor() >= 2) {
//      return '${(difference.inDays / 7).floor()} weeks ago';
      return DateTimeUtils.toYYYYMMDD(date);//(numericDates) ?
    } else if ((difference.inDays / 7).floor() >= 1) {
      return (numericDates) ? '1 week ago' : 'Last week';
    } else if (difference.inDays >= 2) {
      return '${difference.inDays} days ago';
    } else if (difference.inDays >= 1) {
      return (numericDates) ? '1 day ago' : 'Yesterday';
    } else if (difference.inHours >= 2) {//return DateTimeUtils.toYYYYMMDD(date);//(numericDates) ?
      return '${difference.inHours} hours ago';
    } else if (difference.inHours >= 1) {
      return '1 hour ago';// : 'An hour ago';
    } else if (difference.inMinutes >= 2) {
      return '${difference.inMinutes} mins ago';
    } else if (difference.inMinutes >= 1) {
      return (numericDates) ? '1 min ago' : 'A min ago';
    } else if (difference.inSeconds >= 3) {
      return '${difference.inSeconds} seconds ago';
    } else {
      return 'Just now';
    }
  }
}
