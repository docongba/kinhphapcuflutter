import 'dart:io';

import 'package:codebase/constant/assets_constant.dart';
import 'package:codebase/constant/ui_constant.dart';
import 'package:codebase/manager/route/app_navigator.dart';
import 'package:codebase/theme/colors.dart';
import 'package:codebase/theme/custom_decoration.dart';
import 'package:codebase/theme/text_style.dart';
import 'package:codebase/utils/strings.dart';
import 'package:codebase/widget/custom/bk_icon.dart';
import 'package:codebase/widget/custom/cancel_button.dart';
import 'package:codebase/widget/custom/nemo_dialog.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ImagePickerHandler {


  openCamera(Function(File) result) async {
    // var image = await ImagePicker.pickImage(source: ImageSource.camera);
    var image = await ImagePicker().getImage(source: ImageSource.camera, imageQuality: 70, maxWidth: 800, maxHeight: 800);
    if (image != null) {
      // result(File(image.path));
//      cropImage(image, (file) {
        result(File(image.path));
//      });
    }
  }

  openGallery(Function(File) result) async {
    // var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    var image = await ImagePicker().getImage(source: ImageSource.gallery, imageQuality: 70, maxWidth: 800, maxHeight: 800);
    if (image != null) {
      // result(File(image.path));
//      cropImage(image, (file) {
        result(File(image.path));
//      });
    }
  }

Future showPhotoPicker(BuildContext context, Function(File) result) async{
  NemoDialog(
      title: AppString.titleSelectPhoto,
      body: Container(
        height: kDouble_180,
        child: Column(

          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.all(kDouble_10),
//                padding: EdgeInsets.all(kDouble_20),
              child: Column(
                children: [
                  Container(
                    height: kDouble_50,
                    child: FlatButton(

                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(AppString.btnTakePhoto, style: TextStyleCustom.semiBold18(textColor: kTextColor),), SizedBox(width: kDouble_8,), BKIcon(resource: AssetsConstant.ic_camera, width: kDouble_32, height: kDouble_32,)
                        ],
                      ),
                      onPressed: (){
                        openCamera((file) {
                          result(file);
                        }
                        );

                      },
                    ),
                  ),
                  Container(
                    height: 0.5,
                    color: kGreyTextColor,
                  ),
                  Container(
                    height: kDouble_50,
                    child: FlatButton(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(AppString.btnGallery, style: TextStyleCustom.semiBold18(textColor: kTextColor)), SizedBox(width: kDouble_8,), BKIcon(resource: AssetsConstant.ic_gallery, width: kDouble_32, height: kDouble_32,)
                        ],
                      ),
                      onPressed: (){
                        openGallery((file) {
                          result(file);
                        }
                        );
                      },
                    ),
                  ),
                ],
              ),

              decoration: CustomDecoration.tagDecoration(radius: kDouble_10, color: kWhiteColor, borderColor: kPrimaryColor),
            ),
            CancelButton(
              onTap: (){
                AppNavigator().pop();
              },
            )
          ],

        ),
      )).show(context);
}

}

abstract class ImagePickerListener {
  void getImage(File _image);
}
