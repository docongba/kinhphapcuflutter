import 'dart:async';

import 'package:codebase/utils/strings.dart';

const String _kEmailRule = r"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$";
const String _kZipRule = r"^[0-9]{5}(?:-[0-9]{4})?$";
const String _kPhoneRule = r"^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$";

class InputValidator {
//	bool isZipValid = RegExp(r"^[a-z0-9][a-z0-9\- ]{0,10}[a-z0-9]$", caseSensitive: false).hasMatch(zip);

  static bool phoneValidator(String phone) {
    final RegExp zipExp = new RegExp(_kPhoneRule);

    if (zipExp.hasMatch(phone.trim())) {
      return true;
    } else {
			print('phoneValidator fail');
      return false;
    }
  }

  static bool emailValidator(String email) {
    final RegExp zipExp = new RegExp(_kEmailRule);

    if (zipExp.hasMatch(email.trim())) {
      return true;
    } else {
    	print('emailValidator fail');
      return false;
    }
  }

  final StreamTransformer<String, String> validatePhone =
      StreamTransformer<String, String>.fromHandlers(handleData: (zipCode, sink) {
    final RegExp zipExp = new RegExp(_kPhoneRule);

    if (!zipExp.hasMatch(zipCode.trim()) && zipCode.isNotEmpty) {
      sink.addError(AppString.errInvalidPhoneNumber);
    } else {
      sink.add(zipCode);
    }
  });

  final StreamTransformer<String, String> validateZipCode =
      StreamTransformer<String, String>.fromHandlers(handleData: (zipCode, sink) {
    final RegExp zipExp = new RegExp(_kZipRule);

    if (!zipExp.hasMatch(zipCode.trim()) && zipCode.isNotEmpty) {
      sink.addError(AppString.errInvalidZipcode);
    } else {
      sink.add(zipCode);
    }
  });

  final StreamTransformer<String, String> validateEmail =
      StreamTransformer<String, String>.fromHandlers(handleData: (email, sink) {
    final RegExp emailExp = new RegExp(_kEmailRule);

    print('validateEmail $email');
    if (!emailExp.hasMatch(email.trim())) {
      sink.addError(AppString.errInvalidEmail);
    } else if (email.isEmpty) {
      sink.addError(AppString.errEmptyEmail);
    } else {
      sink.add(email);
    }
  });

  final StreamTransformer<String, String> validatePassword =
      StreamTransformer<String, String>.fromHandlers(handleData: (username, sink) {
    if (username.isEmpty) {
      sink.addError(AppString.errEmptyPassword);
    } else {
      print("validatePassword " + username);
      sink.add(username);
    }
  });
}
